//-- CHECKED OK --//
function TogglePage(pzShowHide) {
  var x = $("div.box.xlist-view"), y = $("div.box.xform-tambah-edit");
  if (pzShowHide == false) {
    x.hide();
    y.removeClass("hidden").show();
  } else {
    x.show();
    y.hide();
  }
}
//
function xLoadSatuan() {
  $.fqPost('childs/satuan', null, function (pzdata) {
    if (pzdata.result == 'ERROR') {
      xAlert(pzdata.desc);
    } else {
      $('div.box-body table tbody').html(pzdata.listing);
      //
      $('button.xedit').click(function (event) {
        if (event.stopPropagation) event.stopPropagation();
        else event.cancelBubble = true;
        EditDataShow($(this));
      });
      //
      $('button.xdelete').click(function (event) {
        if (event.stopPropagation) event.stopPropagation();
        else event.cancelBubble = true;
        DeleteDataShow($(this));
      });
    }
  }, true);
}
//
function DeleteDataShow(pzObject) {
  var tTD = pzObject.parent().parent().find('td'),
    tMsg = 'Anda yakin untuk menghapus data satuan : ' + tTD[0].innerText + ' ?';
  xDialog(tMsg, function () {
    $.fqPost('childs/satuan/hapus', {satuan: tTD[0].innerText}, function (pzdata) {
      if (pzdata.result == 'ERROR') {
        xAlert(pzdata.desc);
      } else {
        vxDialog.modal('hide');
        xAlert(pzdata.desc, function () {
          window.location.reload();
        }, 'Hapus Data Satuan Berhasil', 2);
      }
    }, true);
  }, 'Hapus Data Satuan');
}
//
function TambahDataShow() {
  TogglePage(false);
  $("div.xform-tambah-edit h3.box-title").text('Tambah Data Satuan');
  $('#inputOldSatuan').val('');
  $('#inputSatuan').val('').focus();
}
//
function EditDataShow(pzObject) {
  TogglePage(false);
  var tTD = pzObject.parent().parent().find('td');
  $("div.xform-tambah-edit h3.box-title").text('Edit Data Satuan');
  $('#inputOldSatuan').val(tTD[0].innerText);
  $('#inputSatuan').val(tTD[0].innerText).focus();
}
//
$(function () {
  $("#tombolTambah").click(function (event) {
    TambahDataShow();
  });
  $("button.xbatal").click(function (event) {
    TogglePage(true);
  });
  $("div.box.xform-tambah-edit form").submit(function (event) {
    event.preventDefault();
    $.fqPost('childs/satuan/tambahedit', $(this).serialize(), function (pzdata) {
      if (pzdata.result == 'ERROR') {
        xAlert(pzdata.desc);
      } else {
        if ($('#inputRootKode').val() == '') tKet = "Tambah Data Satuan Berhasil";
        else tKet = "Edit Data Satuan Berhasil";
        xAlert(pzdata.desc, function () {
          window.location.reload();
        }, tKet, 2);
      }
    }, true);
  });
  xLoadSatuan();
});
