//-- CHECKED OK --//
var vTable1, vLastIndex = -1, vTEH = 0;
//
function TogglePage()
{
  var fp = $("#frontPage"), pmd = $("#pageMasterDetail");
  if (fp.hasClass("hidden"))
  {
    fp.removeClass("hidden");
    pmd.addClass("hidden");
  } else {
    fp.addClass("hidden");
    pmd.removeClass("hidden");
  }
}
//
function CetakData()
{
  var tdata = vTable1.row({ selected: true}).data(),
      inp1 = $("#inputKode"), inp2 = $("#inputHakAkses"), 
      inp3 = $("#inputUID"), inp4 = $("#inputNama"), 
      inp5 = $("#inputSKPD"), tHak = tdata.hak.split(",");
  if (typeof tdata === 'undefined') tdata = {};
  if (vTEH === 1 || tdata.length === 0){
    inp1.val('');
    inp2.val('');
    inp3.val('');
    inp4.val('');
    inp5.val('');
  } else {
    inp1.val(tdata.kode);
    inp2.val(tdata.hak);
    inp3.val(tdata.uid);
    inp4.val(tdata.nama);
    inp5.val(tdata.kodesatker);
    for (i = 0; i < tHak.length; i++) {
      $("input#"+tHak[i]).prop('checked', true);
    }
  }
}
//
$(function () {
  vTable1 = $("#listusers").DataTable({
    "ajax": "users/listdata",
    "columns": [
      { "data": "uid", "width": "100px" },
      { "data": "nama" },
      { "data": "satker" },
      { "data": null, 
        "width": "250px",
        "className": "text-right",
        "defaultContent": '<button class="btn btn-sm btn-primary xedit"><i class="fa fa-edit"></i> Edit</button>&nbsp;&nbsp;'+
                '<button class="btn btn-sm btn-warning xreset"><i class="fa fa-refresh"></i> Generate Password</button>&nbsp;&nbsp;'+
                '<button class="btn btn-sm btn-danger xdelete"><i class="fa fa-eraser"></i> Hapus</button>'
      }
    ],
    "ordering": false,
    "lengthChange": false,
    "pageLength": 10,
    "pagingType": "numbers",
    "processing": true,
    "select": {
      "style": "single",
      "info": false
    },
    "language": {
      "search": "Cari :",
      "infoFiltered": "(_TOTAL_ dari _MAX_ data)",
      "info": "Halaman _PAGE_ dari _PAGES_ halaman."
    },
    "dom": '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"<"kontrol1 pull-left">fr>'+
      't<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',
    "drawCallback": function(settings){
      if (vTable1) {
        tdata = vTable1.row({ selected: true}).data();
        if (vTable1.data().length > 0 && typeof tdata === 'undefined') vTable1.row(vLastIndex !== -1 ? vLastIndex : 0).select();
        if (vLastIndex !== -1) vLastIndex = -1;
      }
      $("button.xedit").off().click(function(event){
        if (event.stopPropagation) event.stopPropagation();
        else event.cancelBubble = true;
        event.preventDefault();
        var tTR = $(this).closest('TR'); 
        vTable1.row(tTR).select();
        TogglePage();
        CetakData();
      });
      $("button.xreset").off().click(function(event){
        if (event.stopPropagation) event.stopPropagation();
        else event.cancelBubble = true;
        event.preventDefault();
        var tTR = $(this).closest('TR'), tdata = vTable1.row(tTR).data(),
        tMsg = 'Anda yakin untuk me-generate password user ' + tdata.nama + ' ?';
        vTable1.row(tTR).select();
        xDialog(tMsg, function () {
          $.fqPost('childs/users/resetpassword', {kode: tdata.kode}, function (pzdata) {
            if (pzdata.result == 'ERROR') {
              xAlert(pzdata.desc);
            } else {
              vxDialog.modal('hide');
              xAlert(pzdata.desc, null, 'Generate Password User Berhasil', 2);
            }
          }, true);
        }, 'Generate Password User');
      });
      $("button.xdelete").off().click(function(event){
        if (event.stopPropagation) event.stopPropagation();
        else event.cancelBubble = true;
        event.preventDefault();
        var tTR = $(this).closest('TR'), tdata = vTable1.row(tTR).data(),
        tMsg = 'Anda yakin untuk menghapus data user ' + tdata.nama + ' ?';
        vTable1.row(tTR).select();
        xDialog(tMsg, function () {
          $.fqPost('childs/users/hapus', {kode: tdata.kode}, function (pzdata) {
            if (pzdata.result == 'ERROR') {
              xAlert(pzdata.desc);
            } else {
              vxDialog.modal('hide');
              xAlert(pzdata.desc, function () {
                vTable1.ajax.reload();
              }, 'Hapus Data User Berhasil', 2);
            }
          }, true);
        }, 'Hapus Data User');
      });
    }
  }).on('select', function(e, dt, type, indexes) {
    vLastIndex = indexes;
  });
  $("div.kontrol1").html('<button type="button" id="tambahUser" class="btn btn-sm btn-success btn-flat btn-siksda"><i class="fa fa-plus"></i> Tambah User</button>');
  $("#tambahUser").click(function(event){
    $("form#formUser").trigger('reset').find("input[type=hidden]").val('');
    $("div#pageMasterDetail h3.box-title").text('Tambah Data User');
    vTEH = 1;
    TogglePage();
    CetakData();
  });
  $("#tombolKembali").click(function(event){
    vTEH = 0;
    TogglePage();
    vTable1.ajax.reload();
  });
  $("button.xbatal").click(function (event) {
    vTEH = 0;
    TogglePage(true);
  });
  $("form#formUser").submit(function (event) {
    event.preventDefault();
    var tCB = $("input[type='checkbox']:checked"), tHak = '';
    for (i = 0; i < tCB.length; i++) {
      if (tHak.length > 0) tHak += ',';
      tHak += tCB[i].id;
    }
    $('#inputHakAkses').val(tHak);
    $.fqPost('childs/users/tambahedit', $(this).serialize(), function (pzdata) {
      if (pzdata.result == 'ERROR') {
        xAlert(pzdata.desc);
      } else {
        if ($('#inputKode').val() == '') tKet = "Tambah Data User Berhasil";
        else tKet = "Edit Data User Berhasil";
        xAlert(pzdata.desc, function () {
          window.location.reload();
        }, tKet, 2);
      }
    }, true);
  });
});
