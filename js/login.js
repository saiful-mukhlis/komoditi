/***********************************************
 ***   NEW RECONSTRUCTION DONE - 24/10/2017  ***
 ***********************************************/
var _o = {
  init: function(){
    ///////////////////////////////////////
    // public variable
    var _s = this;
    ///////////////////////////////////////
    // button
    _s.btn1 = $("#tombolKembali").click(function(){
      $.fqPost(xpathctrl+'backxperiode', null, function(){ 
        window.location.reload(); 
      }, true);
    });
    ///////////////////////////////////////
    // input
    _s.inp1 = $("#inputUID").focus(function(){
      $(this).val('');
      return false;
    });
    _s.inp2 = $("#inputPassword").focus(function(){
      $(this).val('');
      return false;
    });
    ///////////////////////////////////////
    // form
    _s.form1 = $("form#formXPeriode").submit(function(event){
      event.preventDefault();
      $.fqPost(xpathctrl+'xperiode', $(this).serialize(), function(){ 
        window.location.reload(); 
      }, true);
    });
    _s.form2 = $("form#formLogin").submit(function(event){
      event.preventDefault();
      var tUID = _s.inp1.val(), tPass = _s.inp2.val();
      if (tUID === '') {
        xAlert('UID masih kosong...', function () {
          _s.inp1.focus();
        });
      } else if (tPass === '') {
        xAlert('Password masih kosong...', function () {
          _s.inp2.focus();
        });
      } else {
        $.fqPost(xpathctrl+'proclogin', _s.form2.serialize(), function (pzdata) {
          if (pzdata.result === 'ERROR') {
            xAlert(pzdata.desc);
          } else {
            window.location.reload();
          }
        }, true);
      }
    });
    setTimeout(function () {
      _s.inp1.focus();
      _s.inp2.val('');
    }, 100);    
  }
};
$(function () { _o.init(); });
