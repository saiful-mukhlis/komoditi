var _o = {
  init: function(){
    ///////////////////////////////////////
    // public variable
    var _s = this;
    _s.teh = 0;
    _s.data = null;
    _s.path = 'report/';
      _s.kode = '';
    ///////////////////////////////////////
    // dialog
    _s.dlg1 = $("div.modal.xtambah-edit-pasar").on('shown.bs.modal', function(){
      _s.inp2.focus().select();
    });
    ///////////////////////////////////////
    // list table (DataTable)
    _s.list1 = $("#listdata").DataTable({
      "serverSide": true,
      "ajax": {
        "url": _s.path+"listdata",
        "type": "POST"
      },
      "columns": [
        { "data": "kode" },
        { "data": "tanggal" },
        { "data": null,
          "searchable": false,
          "className": "text-right",
          "render": function (data, type, full, meta) {
              return '<a href="reporttampilan?kode='+full.kode+'" type="button" class="btn btn-sm btn-primary btn-flat xbtnview"><i class="fa fa-eye"></i> Tampilkan</a> <button type="button" class="btn btn-sm btn-primary btn-flat xbtnedit"><i class="fa fa-edit"></i> Edit</button> <button type="button" class="btn btn-sm btn-danger btn-flat xbtndelete"><i class="fa fa-delete"></i> Delete</button>';
          },
          "defaultContent": '<a href="reporttampilan" type="button" class="btn btn-sm btn-primary btn-flat xbtnview"><i class="fa fa-eye"></i> Tampilkan</a> <button type="button" class="btn btn-sm btn-primary btn-flat xbtnedit"><i class="fa fa-edit"></i> Edit</button> <button type="button" class="btn btn-sm btn-danger btn-flat xbtndelete"><i class="fa fa-delete"></i> Delete</button>'
        }
      ],
      "ordering": false,
      "searching": true,
      "info": true,
      "lengthChange": false,
      "paging": false,
      "processing": true,
      "scrollY": 350,
      "select": {
        "style": "single",
        "info": false
      },
      "dom": '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"<"kontrol1 pull-left">fr>'+
        't<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',
      "drawCallback": function(settings){
        if (_s.list1) {
          $("button.xbtnedit").off().click(function(event){
            if (event.stopPropagation) event.stopPropagation();
            else event.cancelBubble = true;
            var tr = $(this).closest('TR');
            _s.data = _s.list1.rows(tr).data()[0];
            _s.list1.rows(tr).select();
            _s.teh = 2;
            _s.title.html('Edit Pasar');
            _s.inp1.val(_s.data.kode);
            _s.inp2.val(_s.data.nama);
              _s.inp3.val(_s.data.alamat);
            _s.inp4.val(_s.data.kode);
            _s.dlg1.modal({backdrop: 'static', show: true});
          });
            /// Hapus
            $("button.xbtndelete").off().click(function(event){
                if (event.stopPropagation) event.stopPropagation();
                else event.cancelBubble = true;
                var tr = $(this).closest('TR');
                var tdata = _s.list1.rows(tr).data()[0];
                tQ = 'Anda yakin untuk menghapus Pasar '+tdata.nama+' ?';
                xDialog(tQ, function(){
                    $.fqPost(_s.path+"hapus", {kode: tdata.kode}, function (pzdata) {
                        vxDialog.modal('hide');
                        if (pzdata.result == 'ERROR') {
                            xAlert(pzdata.desc);
                        } else {
                            xAlert(pzdata.desc, function () {
                                _s.list1.ajax.reload();
                            }, 'Hapus Pasar', 2);
                        }
                    }, true);
                }, 'Hapus Pasar');
            });
        }
      }
    }).on('xhr.dt', function(e, setting, json, xhr){
      _s.teh = 0;
        _s.kode = json.kode
    });

    ///////////////////////////////////////
    // button

    ///////////////////////////////////////
    // input

    ///////////////////////////////////////
    // other
    _s.title = $('div.modal.xtambah-edit-pasar h4.modal-title');
  },
    cetakdata: function(){
        var _s = this;
        if (_s.teh == 1) {
            _s.form1.trigger('reset').find("input[type=hidden]").val('');
            _s.inp1.val(_s.nextid);
            _s.inp2.focus();
        }
        _s.setkontrol();
    }
};
$(function () { _o.init(); });
