var _o = {
  init: function(){
    ///////////////////////////////////////
    // public variable
    var _s = this;
    _s.teh = 0;
    _s.data = null;
    _s.path = 'bank/';
    ///////////////////////////////////////
    // dialog
    _s.dlg1 = $("div.modal.xtambah-edit-bank").on('shown.bs.modal', function(){
      _s.inp1.focus().select();
    });
    ///////////////////////////////////////
    // list table (DataTable)
    _s.list1 = $("#listdata").DataTable({
      "serverSide": true,
      "ajax": {
        "url": _s.path+"listdata",
        "type": "POST"
      },
      "columns": [
        { "data": "kode" },
        { "data": "nama" },
        { "data": null,
          "searchable": false,
          "className": "text-right",
          "defaultContent": '<button type="button" class="btn btn-sm btn-primary btn-flat xbtnedit"><i class="fa fa-edit"></i> Edit</button>'
        }
      ],
      "ordering": false,
      "searching": true,
      "info": true,
      "lengthChange": false,
      "paging": false,
      "processing": true,
      "scrollY": 350,
      "select": {
        "style": "single",
        "info": false
      },
      "dom": '<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-tl ui-corner-tr"<"kontrol1 pull-left">fr>'+
        't<"fg-toolbar ui-toolbar ui-widget-header ui-helper-clearfix ui-corner-bl ui-corner-br"ip>',
      "drawCallback": function(settings){
        if (_s.list1) {
          $("button.xbtnedit").off().click(function(event){
            if (event.stopPropagation) event.stopPropagation();
            else event.cancelBubble = true;
            var tr = $(this).closest('TR');
            _s.data = _s.list1.rows(tr).data()[0];
            _s.list1.rows(tr).select();
            _s.teh = 2;
            _s.title.html('Edit Bank');
            _s.inp1.val(_s.data.kode);
            _s.inp2.val(_s.data.nama);
            _s.inp3.val(_s.data.kode);
            _s.dlg1.modal({backdrop: 'static', show: true});
          });
        }
      }
    }).on('xhr.dt', function(e, setting, json, xhr){
      _s.teh = 0;
    });
    $("div.kontrol1").html('<button type="button" id="tambahBank" class="btn btn-sm btn-success btn-flat"><i class="fa fa-plus"></i> Tambah Bank</button>');
    ///////////////////////////////////////
    // button
    _s.btn1 = $("#tambahBank").click(function(){
      _s.teh = 1;
      _s.title.html('Tambah Bank');
      _s.inp1.val('');
      _s.inp2.val('');
      _s.inp3.val('');
      _s.dlg1.modal({backdrop: 'static', show: true});
    });
    ///////////////////////////////////////
    // input
    _s.inp1 = $("#inputKode").inputmask('999');
    _s.inp2 = $("#inputBank");
    _s.inp3 = $("#idedit");
    ///////////////////////////////////////
    // form
    _s.form1 = $("form#formTambahEditBank").submit(function(event){
      event.preventDefault();
      $.fqPost(_s.path+'tambaheditbank', _s.form1.serialize(), function(pzdata){
        _s.dlg1.modal('hide');
        if (pzdata.result === 'ERROR') {
          xAlert(pzdata.desc);
        } else {
          _s.list1.ajax.reload();
        }
      }, true);
    });
    ///////////////////////////////////////
    // other
    _s.title = $('div.modal.xtambah-edit-bank h4.modal-title');
  }
};
$(function () { _o.init(); });
