/***********************************************
 ***   NEW RECONSTRUCTION DONE - 24/10/2017  ***
 ***********************************************/
$(function (){ 
    ///////////////////////////////////////
    // public variable
    var _s = this;
    _s.path = xpath+'dashboard/';
    ///////////////////////////////////////
    // dialog
    _s.dlg1 = $("#xchange-password");
    ///////////////////////////////////////
    // button
    _s.btn1 = $("#xLogout").click(function(){
      $.fqPost(_s.path+'logout', null, function (pzdata) {
        window.location.reload();
      }, true);
    });
    _s.btn2 = $("#xChPassword").click(function(){
      _s.dlg1.modal({backdrop: 'static', show: true})
        .on('shown.bs.modal', function (event) {
          _s.inp3.val('');
          _s.inp2.val('');
          _s.inp1.val('').focus();
        })
    });
    ///////////////////////////////////////
    // input
    _s.inp1 = $("#inputPasswordLama");
    _s.inp2 = $("#inputPasswordBaru");
    _s.inp3 = $("#inputKonfirmasi");
    ///////////////////////////////////////
    // form
    _s.form1 = $("form#xchange-password-form").submit(function(event){
      event.preventDefault();
      $.fqPost(_s.path+'updatepassword', $(this).serialize(), function (pzdata) {
        if (pzdata.result == 'ERROR') {
          xAlert(pzdata.desc);
        } else {
          _s.dlg1.modal('hide');
          xAlert(pzdata.desc, null, 'Ganti Password', 2);
        }
      }, true);
    });
});
