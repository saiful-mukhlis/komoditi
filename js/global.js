//-- CHECKED OK --//
//////////////////////////////////////////////
// XALERT
var vxAlert = null;
//
function xAlert(pzMessage, pzReturn, pzTitle, pzMode) {
  var xclass = ['modal-warning', 'modal-danger', 'modal-success', 'modal-info'];
  if (!pzMode) pzMode = 1;
  if (!pzTitle) pzTitle = 'Error';
  if (vxAlert === null) {
    vxAlert = $('<div class="modal fade" id="vxAlert" tabindex="-1" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title">Error</h4></div><div class="modal-body"><button type="button" class="btn btn-default btn-sm btn-flat pull-right" data-dismiss="modal">OK</button><p></p></div></div></div></div>');
    vxAlert.appendTo("body");
  }
  vxAlert.removeClass('modal-warning modal-danger modal-success modal-info');
  vxAlert.addClass(xclass[pzMode]);
  vxAlert.find('.modal-title').text(pzTitle);
  vxAlert.find('.modal-body p').html(pzMessage);
  vxAlert.modal('show');
  vxAlert.off('hidden.bs.modal');
  if (pzReturn) vxAlert.on('hidden.bs.modal', pzReturn);
}

//////////////////////////////////////////////
// LOADING VARIABLE
var vLoading = null;
//////////////////////////////////////////////
// LOADING SHOW
function xLoadingShow() {
  if (vLoading === null)
    vLoading = $('<div id="vLoading" class="screenblocker"><div class="spinner"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div><div class="rect4"></div><div class="rect5"></div></div></div>');
  vLoading.appendTo("body");
}
//////////////////////////////////////////////
// LOADING HIDE
function xLoadingHide() {
  vLoading = $("#vLoading.screenblocker").detach();
};

//////////////////////////////////////////////
// XDIALOG
var vxDialog = null;
//
function xDialog(pzMessage, pzReturn, pzTitle, pzMode) {
  var xclass = ['', 'modal-warning', 'modal-danger', 'modal-success', 'modal-info'];
  if (!pzMode) pzMode = 4;
  if (!pzTitle) pzTitle = 'Dialog';
  if (vxDialog === null) {
    vxDialog = $('<div class="modal fade" id="vxDialog" tabindex="-1" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 class="modal-title"></h4></div><div class="modal-body"><button type="button" class="btn btn-default btn-sm btn-flat pull-right" data-dismiss="modal">Cancel</button><button type="button" class="btn btn-danger btn-sm btn-space btn-flat pull-right" id="vxDialogOK" style="margin-right: 5px;">OK</button><p></p></div></div></div></div>');
    vxDialog.appendTo("body");
  }
  vxDialog.removeClass('modal-warning modal-danger modal-success modal-info');
  vxDialog.addClass(xclass[pzMode]);
  vxDialog.find('.modal-title').text(pzTitle);
  vxDialog.find('.modal-body p').html(pzMessage);
  vxDialog.modal('show');
  $("#vxDialogOK").unbind("click");
  if (pzReturn) $("#vxDialogOK").click(pzReturn);
}

//////////////////////////////////////////////
// CUSTOM AJAX POST
$.fqPost = function (pzAction, pzData, pzReturn, pzLoadIndi) {
  if (pzLoadIndi === true) xLoadingShow();
  $.ajax({
    method: 'POST',
    url: pzAction,
    data: pzData,
    timeout: 600000,
    dataType: 'json'
  }).done(pzReturn).error(function (jqXHR, textStatus, errorThrown) {
    xAlert(textStatus+'<br/>'+errorThrown);
  }).always(function () {
    if (pzLoadIndi === true) xLoadingHide();
  });
};

/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function (n, x, s, c) {
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
    num = this.toFixed(Math.max(0, ~~n));
  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};
Number.prototype.formatx = function (n, x, s, c) {
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
    num = this.toFixed(Math.max(0, ~~n));
  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

Date.prototype.format = function(format) {
  // set default format if function argument not provided
  format = format || 'YYYY-MM-DD hh:mm';
  var zeropad = function(number, length) {
    number = number.toString();
    length = length || 2;
    while(number.length < length)
        number = '0' + number;
    return number;
  },
  // here you can define your formats
  formats = {
    YYYY: this.getFullYear(),
    MM: zeropad(this.getMonth() + 1),
    DD: zeropad(this.getDate()),
    hh: zeropad(this.getHours()),
    mm: zeropad(this.getMinutes())
  },
  pattern = '(' + Object.keys(formats).join(')|(') + ')';
  return format.replace(new RegExp(pattern, 'g'), function(match) {
    return formats[match];
  });
};

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  //
  var url = window.location, thref = url.href.replace('#', '').replace(/\?.+/, '');
  var element = $('.sidebar ul.sidebar-menu > li a').filter(function () {
    var xhref = this.getAttribute('href').replace(/\?.+/, '');
    return (xhref === thref && xhref !== '#');
  }).parents('li').addClass('active');
});
