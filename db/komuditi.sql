-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table dbkomoditi.tmlogkomoditi
CREATE TABLE IF NOT EXISTS `tmlogkomoditi` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kodesatuan` varchar(50) NOT NULL,
  `kodekelompok` varchar(50) NOT NULL,
  `staktif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`kode`),
  KEY `FK_Komoditi_Satuan` (`kodesatuan`),
  KEY `FK_Komoditi_Kelompok` (`kodekelompok`),
  CONSTRAINT `FK_Komoditi_Kelompok` FOREIGN KEY (`kodekelompok`) REFERENCES `tmlogreference` (`kode`),
  CONSTRAINT `FK_Komoditi_Satuan` FOREIGN KEY (`kodesatuan`) REFERENCES `tmlogreference` (`kode`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbkomoditi.tmlogkomoditi: ~4 rows (approximately)
/*!40000 ALTER TABLE `tmlogkomoditi` DISABLE KEYS */;
INSERT INTO `tmlogkomoditi` (`kode`, `nama`, `kodesatuan`, `kodekelompok`, `staktif`) VALUES
	('BRG001', 'Beras Bengawan', 'S04', 'K01', 1),
	('BRG002', 'Beras metik', 'S04', 'K01', 1),
	('BRG003', 'Beras IR64', 'S04', 'K01', 1),
	('BRG004', 'Gula Pasir Dalam Negeri', 'S04', 'K02', 1);
/*!40000 ALTER TABLE `tmlogkomoditi` ENABLE KEYS */;

-- Dumping structure for table dbkomoditi.tmlogreference
CREATE TABLE IF NOT EXISTS `tmlogreference` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `tipe` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbkomoditi.tmlogreference: ~39 rows (approximately)
/*!40000 ALTER TABLE `tmlogreference` DISABLE KEYS */;
INSERT INTO `tmlogreference` (`kode`, `nama`, `tipe`) VALUES
	('K01', 'BERAS', 0),
	('K02', 'GULA PASIR', 0),
	('K03', 'MINYAK GORENG', 0),
	('K04', 'DAGING', 0),
	('K05', 'TELUR AYAM', 0),
	('K06', 'SUSU', 0),
	('K07', 'JAGUNG PIPILAN KERING', 0),
	('K08', 'GARAM BERYODIUM', 0),
	('K09', 'TEPUNG TERIGU', 0),
	('K10', 'KACANG KEDELAI', 0),
	('K11', 'MIE INSTANT', 0),
	('K12', 'CABE', 0),
	('K13', 'BAWANG', 0),
	('K14', 'IKAN ASIN TERI', 0),
	('K15', 'KACANG HIJAU', 0),
	('K16', 'KACANG TANAH', 0),
	('K17', 'KETELA POHON', 0),
	('K18', 'SAYUR MAYUR', 0),
	('K19', 'SEMEN', 0),
	('K20', 'IKAN SEGAR', 0),
	('K21', 'KOSONG', 0),
	('K22', 'KAYU BALOK MERANTI (4 X 10)', 0),
	('K23', 'PAPAN MERANTI (4m X 3cm X 20mm)', 0),
	('K24', 'TRIPLEK (6MM)', 0),
	('K25', 'BESI BETON (SNI MURNI)', 0),
	('K26', 'PAKU', 0),
	('K27', 'GAS ELPIGI 3 Kg', 0),
	('K28', 'PUPUK', 0),
	('S01', 'Buah', 1),
	('S02', 'Bungkus', 1),
	('S03', 'Btg', 1),
	('S04', 'Kg', 1),
	('S05', 'Lembar', 1),
	('S06', '2 Liter', 1),
	('S07', '390 gr/kl', 1),
	('S08', '397 gr/kl', 1),
	('S09', '40 Kg', 1),
	('S10', '400 gr/dos', 1),
	('S11', '620 ml', 1);
/*!40000 ALTER TABLE `tmlogreference` ENABLE KEYS */;

-- Dumping structure for table dbkomoditi.tmorgpasar
CREATE TABLE IF NOT EXISTS `tmorgpasar` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(250) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbkomoditi.tmorgpasar: ~6 rows (approximately)
/*!40000 ALTER TABLE `tmorgpasar` DISABLE KEYS */;
INSERT INTO `tmorgpasar` (`kode`, `nama`, `alamat`) VALUES
	('P01', 'Pasar  Besar', 'Jalan Kyai Tamin, No. 1A'),
	('P02', 'Pasar Klojen', 'Jalan Cokroaminoto'),
	('P03', 'Pasar Oro-Oro Dowo', 'Jalan Guntur'),
	('P04', 'Pasar Tawang Mangu', 'Jalan Tawang Mangu No. 1'),
	('P05', 'Pasar Madyopuro', 'Jalan Simpang Danau Jonge  Sawojajar'),
	('P06', 'Pasar Blimbing', 'Jalan Borobudur');
/*!40000 ALTER TABLE `tmorgpasar` ENABLE KEYS */;

-- Dumping structure for table dbkomoditi.tmpetugas
CREATE TABLE IF NOT EXISTS `tmpetugas` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `uid` varchar(50) NOT NULL,
  `pwd` varchar(150) NOT NULL,
  `hak` varchar(100) NOT NULL,
  `stbatal` tinyint(1) NOT NULL DEFAULT '0',
  `aktif` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table dbkomoditi.tmpetugas: ~2 rows (approximately)
/*!40000 ALTER TABLE `tmpetugas` DISABLE KEYS */;
INSERT INTO `tmpetugas` (`kode`, `nama`, `uid`, `pwd`, `hak`, `stbatal`, `aktif`) VALUES
	('atoksub', 'hardjianto subekti', 'atoksub', '8ef4ea27f996cadb3a0dba2043c31190', '1111111111111111111111111111111111111', 0, 1),
	('test', 'test', 'test', 'b4ffad553249bc8d36abd5683bc7f8f2', '111111111111111111111111111111', 0, 1);
/*!40000 ALTER TABLE `tmpetugas` ENABLE KEYS */;

-- Dumping structure for table dbkomoditi.ttkomiditidetail
CREATE TABLE IF NOT EXISTS `ttkomiditidetail` (
  `kode` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kodetrans` int(10) unsigned DEFAULT NULL,
  `kodekomoditi` varchar(50) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` decimal(10,0) NOT NULL,
  PRIMARY KEY (`kode`),
  KEY `kodetrans` (`kodetrans`),
  KEY `kodekomoditi` (`kodekomoditi`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table dbkomoditi.ttkomiditidetail: ~4 rows (approximately)
/*!40000 ALTER TABLE `ttkomiditidetail` DISABLE KEYS */;
INSERT INTO `ttkomiditidetail` (`kode`, `kodetrans`, `kodekomoditi`, `stok`, `harga`) VALUES
	(1, 1, 'BRG001', 10, 10000),
	(2, 1, 'BRG002', 10, 10000),
	(3, 1, 'BRG003', 10, 10000),
	(4, 1, 'BRG004', 10, 10000);
/*!40000 ALTER TABLE `ttkomiditidetail` ENABLE KEYS */;

-- Dumping structure for table dbkomoditi.ttkomoditi
CREATE TABLE IF NOT EXISTS `ttkomoditi` (
  `kode` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tanggal` datetime NOT NULL,
  `kodepasar` varchar(50) NOT NULL,
  PRIMARY KEY (`kode`),
  KEY `kodepasar` (`kodepasar`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table dbkomoditi.ttkomoditi: ~1 rows (approximately)
/*!40000 ALTER TABLE `ttkomoditi` DISABLE KEYS */;
INSERT INTO `ttkomoditi` (`kode`, `tanggal`, `kodepasar`) VALUES
	(1, '2017-12-06 00:00:00', 'P01');
/*!40000 ALTER TABLE `ttkomoditi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
