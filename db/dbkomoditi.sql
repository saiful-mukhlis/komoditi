-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2017 at 06:13 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbkomoditi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tmlogkomoditi`
--

CREATE TABLE `tmlogkomoditi` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `kodesatuan` varchar(50) NOT NULL,
  `kodekelompok` varchar(50) NOT NULL,
  `staktif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tmlogkomoditi`
--

INSERT INTO `tmlogkomoditi` (`kode`, `nama`, `kodesatuan`, `kodekelompok`, `staktif`) VALUES
('BRG001', 'Beras Bengawan', 'S04', 'K01', 1),
('BRG002', 'Beras metik', 'S04', 'K01', 1),
('BRG003', 'Beras IR64', 'S04', 'K01', 1),
('BRG004', 'Gula Pasir Dalam Negeri', 'S04', 'K02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tmlogreference`
--

CREATE TABLE `tmlogreference` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `tipe` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tmlogreference`
--

INSERT INTO `tmlogreference` (`kode`, `nama`, `tipe`) VALUES
('K01', 'BERAS', 0),
('K02', 'GULA PASIR', 0),
('K03', 'MINYAK GORENG', 0),
('K04', 'DAGING', 0),
('K05', 'TELUR AYAM', 0),
('K06', 'SUSU', 0),
('K07', 'JAGUNG PIPILAN KERING', 0),
('K08', 'GARAM BERYODIUM', 0),
('K09', 'TEPUNG TERIGU', 0),
('K10', 'KACANG KEDELAI', 0),
('K11', 'MIE INSTANT', 0),
('K12', 'CABE', 0),
('K13', 'BAWANG', 0),
('K14', 'IKAN ASIN TERI', 0),
('K15', 'KACANG HIJAU', 0),
('K16', 'KACANG TANAH', 0),
('K17', 'KETELA POHON', 0),
('K18', 'SAYUR MAYUR', 0),
('K19', 'SEMEN', 0),
('K20', 'IKAN SEGAR', 0),
('K21', 'KOSONG', 0),
('K22', 'KAYU BALOK MERANTI (4 X 10)', 0),
('K23', 'PAPAN MERANTI (4m X 3cm X 20mm)', 0),
('K24', 'TRIPLEK (6MM)', 0),
('K25', 'BESI BETON (SNI MURNI)', 0),
('K26', 'PAKU', 0),
('K27', 'GAS ELPIGI 3 Kg', 0),
('K28', 'PUPUK', 0),
('S01', 'Buah', 1),
('S02', 'Bungkus', 1),
('S03', 'Btg', 1),
('S04', 'Kg', 1),
('S05', 'Lembar', 1),
('S06', '2 Liter', 1),
('S07', '390 gr/kl', 1),
('S08', '397 gr/kl', 1),
('S09', '40 Kg', 1),
('S10', '400 gr/dos', 1),
('S11', '620 ml', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tmorgpasar`
--

CREATE TABLE `tmorgpasar` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tmorgpasar`
--

INSERT INTO `tmorgpasar` (`kode`, `nama`, `alamat`) VALUES
('P01', 'Pasar  Besar', 'Jalan Kyai Tamin, No. 1A'),
('P02', 'Pasar Klojen', 'Jalan Cokroaminoto'),
('P03', 'Pasar Oro-Oro Dowo', 'Jalan Guntur'),
('P04', 'Pasar Tawang Mangu', 'Jalan Tawang Mangu No. 1'),
('P05', 'Pasar Madyopuro', 'Jalan Simpang Danau Jonge  Sawojajar'),
('P06', 'Pasar Blimbing', 'Jalan Borobudur');

-- --------------------------------------------------------

--
-- Table structure for table `tmpetugas`
--

CREATE TABLE `tmpetugas` (
  `kode` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `uid` varchar(50) NOT NULL,
  `pwd` varchar(150) NOT NULL,
  `hak` varchar(100) NOT NULL,
  `stbatal` tinyint(1) NOT NULL DEFAULT '0',
  `aktif` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tmpetugas`
--

INSERT INTO `tmpetugas` (`kode`, `nama`, `uid`, `pwd`, `hak`, `stbatal`, `aktif`) VALUES
('atoksub', 'hardjianto subekti', 'atoksub', '8ef4ea27f996cadb3a0dba2043c31190', '1111111111111111111111111111111111111', 0, 1),
('test', 'test', 'test', 'b4ffad553249bc8d36abd5683bc7f8f2', '111111111111111111111111111111', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ttkomiditidetail`
--

CREATE TABLE `ttkomiditidetail` (
  `kode` varchar(50) NOT NULL,
  `kodetrans` varchar(50) NOT NULL,
  `kodekomoditi` varchar(50) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ttkomoditi`
--

CREATE TABLE `ttkomoditi` (
  `kode` varchar(50) NOT NULL,
  `tanggal` datetime NOT NULL,
  `kodepasar` varchar(50) NOT NULL,
  `kodepetugas` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tmlogkomoditi`
--
ALTER TABLE `tmlogkomoditi`
  ADD PRIMARY KEY (`kode`),
  ADD KEY `FK_Komoditi_Satuan` (`kodesatuan`),
  ADD KEY `FK_Komoditi_Kelompok` (`kodekelompok`);

--
-- Indexes for table `tmlogreference`
--
ALTER TABLE `tmlogreference`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tmorgpasar`
--
ALTER TABLE `tmorgpasar`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tmpetugas`
--
ALTER TABLE `tmpetugas`
  ADD PRIMARY KEY (`kode`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tmlogkomoditi`
--
ALTER TABLE `tmlogkomoditi`
  ADD CONSTRAINT `FK_Komoditi_Kelompok` FOREIGN KEY (`kodekelompok`) REFERENCES `tmlogreference` (`kode`),
  ADD CONSTRAINT `FK_Komoditi_Satuan` FOREIGN KEY (`kodesatuan`) REFERENCES `tmlogreference` (`kode`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
