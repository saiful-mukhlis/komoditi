<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('datatablepaging'))
{
	function datatablepaging($pzQuery)
	{
		$CI =& get_instance();
    $POST = $CI->input->post();
    //log_message("INFO", print_r($POST, TRUE));
    $RESULT = array();
    $RESULT['data'] = array();
    ////   Query Modifire
    preg_match("/^select(.*?)from(?(?=.*where.*)(.*)where((?(?=.*order by.*)(.*)order by(.*)|(.*)))|(?(?=.*order by.*)(.*)order by(.*)|(.*)))/is", $pzQuery, $tQP);
    $SELECT = trim($tQP[1]);
    $FROM = trim(!empty($tQP[2])?$tQP[2]:(!empty($tQP[7])?$tQP[7]:$tQP[9]));
    $WHERE = trim(!empty($tQP[6])?$tQP[6]:(!empty($tQP[4])?$tQP[4]:""));
    $ORDER = trim(!empty($tQP[8])?$tQP[8]:(!empty($tQP[5])?$tQP[5]:""));
    if (!empty($SELECT) && !empty($FROM)) {
      $FIELDS = preg_split("/,(?=(([^']*'){2})*[^']*$)/", $SELECT);
      $FILTER = "";
      if (!empty($POST['search']['value'])) {
        foreach ($POST['columns'] as $Column) {
          if ($Column['searchable'] === 'true' && !empty($Column['data'])) {
            $tFieldFound = "";
            foreach($FIELDS as $Field) {
              $tpos = strripos($Field, $Column['data']);
              if ($tpos !== FALSE) {
                preg_match("/(?=.*\sas.\s*)(.*)\sas|(.*)/is", $Field, $tFPart);
                $tFieldFound = trim(!empty($tFPart[1])?$tFPart[1]:$tFPart[2]);
                break;
              }
            }
            if (!empty($tFieldFound)) {
              if (!empty($FILTER)) $FILTER .= " OR ";
              $FILTER .= "({$tFieldFound} LIKE '%{$POST['search']['value']}%')";
            }
          }
        }
      }
      if (!empty($FILTER)) $FILTER = "({$FILTER})";
      ///////
      $ORDERBY = $ORDER;
      if (isset($POST['order'])) {
        $tcolindex = $POST['order'][0]['column'];
        if (!empty($POST['columns'][$tcolindex]['data']) && $POST['columns'][$tcolindex]['orderable'] === 'true') {
          $tOrderField = $POST['columns'][$tcolindex]['data'];
          $tFieldFound = "";
          foreach($FIELDS as $Field) {
            $tpos = strripos($Field, $tOrderField);
            if ($tpos !== FALSE) {
              preg_match("/(?=.*\sas.\s*)(.*)\sas|(.*)/is", $Field, $tFPart);
              $tFieldFound = trim(!empty($tFPart[1])?$tFPart[1]:$tFPart[2]);
              break;
            }
          }
          if (!empty($tFieldFound)) {
            $POST['order'][0]['dir'] = strtoupper($POST['order'][0]['dir']);
            $ORDERBY = $tFieldFound." {$POST['order'][0]['dir']}";
          }
        }
      }
      ///////
      $WHEREALL = "";
      $WHEREFILTER = "";
      if (!empty($WHERE) || !empty($FILTER)) {
        $WHEREALL = (!empty($WHERE)?" WHERE {$WHERE}":"");
        $WHEREFILTER = " WHERE".(!empty($WHERE)?" {$WHERE}":"").(!empty($WHERE)&&!empty($FILTER)?" AND":"").(!empty($FILTER)?" {$FILTER}":"");
      }
      //////
      $QueryCOUNT = "SELECT COUNT(*) AS xcount FROM {$FROM}{$WHEREALL}";
      $QueryCOUNTF = "SELECT COUNT(*) AS xcount FROM {$FROM}{$WHEREFILTER}";
      $QueryDATA = "SELECT {$SELECT} FROM {$FROM}{$WHEREFILTER}".(!empty($ORDERBY)?" ORDER BY {$ORDERBY}":"").((int)$POST['length'] > -1 ?" LIMIT {$POST['start']}, {$POST['length']}":"");
      log_message("INFO", "!!~~~~~~~~~~>> {$QueryCOUNT}");
      log_message("INFO", "!!~~~~~~~~~~>> {$QueryCOUNTF}");
      log_message("INFO", "!!~~~~~~~~~~>> {$QueryDATA}");
      //////
      // Total Record
      $RESULT['recordsTotal'] = $CI->db->query($QueryCOUNT)->row()->xcount;
      // Total Filtered Record
      $RESULT['recordsFiltered'] = $CI->db->query($QueryCOUNTF)->row()->xcount;
      // Data
      $RESULT['data'] = $CI->db->query($QueryDATA)->result();
    }
    $RESULT['draw'] = $POST['draw'];
    return $RESULT;
	}
}
