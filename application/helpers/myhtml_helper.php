<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('CSSList'))
{
  function CSSList()
  {
		$CI =& get_instance();
    $tResult = "";
    $tCSSGlobal = "";
    $tCSSCtrl = "";
    foreach ($CI->data["cssextra"] as $xcss) {
      if (strpos($xcss, "global.css") !== FALSE) {
        $tCSSGlobal = $xcss;
      } elseif (strpos($xcss, "{$CI->ctrl}.css") !== FALSE) {
        $tCSSCtrl = $xcss;
      } else {
        if (!empty($tResult)) $tResult .= PHP_EOL;
        $tResult .= "<link rel=\"stylesheet\" href=\"".base_url($xcss)."\">";
      }
    }
    if (!empty($tCSSGlobal)) {
      if (!empty($tResult)) $tResult .= PHP_EOL;
      $tResult .= "<link rel=\"stylesheet\" href=\"".base_url($tCSSGlobal)."\">";
    }
    if (!empty($tCSSCtrl)) {
      if (!empty($tResult)) $tResult .= PHP_EOL;
      $tResult .= "<link rel=\"stylesheet\" href=\"".base_url($tCSSCtrl)."\">";
    }
    $tResult .= PHP_EOL;
    return $tResult;
  }
}

if (!function_exists('JavascriptList'))
{
  function JavascriptList()
  {
		$CI =& get_instance();
    $tResult = "";
    $tJSCtrl = "";
    foreach ($CI->data["jsextra"] as $xjs) {
      if (strpos($xjs, "{$CI->ctrl}.js") !== FALSE) {
        $tJSCtrl = $xjs;
      } else {
        if (!empty($tResult)) $tResult .= PHP_EOL;
        $tResult .= "<script type=\"text/javascript\" src=\"".base_url($xjs)."\"></script>";
        if (strpos($xjs, "bootstrap.min.js") !== FALSE) {
          $tResult .= PHP_EOL;
          //$tResult .= "<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->".PHP_EOL;
          //$tResult .= "<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->".PHP_EOL;
          $tResult .= "<!--[if lt IE 9]>".PHP_EOL;
          $tResult .= "<script type=\"text/javascript\" src=\"".base_url("assets/html5shiv/html5shiv.min.js")."\"></script>".PHP_EOL;
          $tResult .= "<script type=\"text/javascript\" src=\"".base_url("assets/respond/respond.min.js")."\"></script>".PHP_EOL;
          $tResult .= "<![endif]-->";
        }
      }
    }
    if (!empty($tJSCtrl)) {
      if (!empty($tResult)) $tResult .= PHP_EOL;
      $tResult .= "<script type=\"text/javascript\" src=\"".base_url($tJSCtrl)."\"></script>";
    }
    $tResult .= PHP_EOL;
    return $tResult;
  }
}
