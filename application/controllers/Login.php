<?php
/***********************************************
 ***   NEW RECONSTRUCTION DONE - 24/10/2017  ***
 ***********************************************/
defined("BASEPATH") OR exit("No direct script access allowed");

class Login extends MY_Xnonmember
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model("{$this->pathctrl}_model", "xmdl");
  }
  //
  public function index()
  {
    $this->data["jsextra"][] = "js/{$this->pathctrl}.js?_=".time();
    $this->load->view($this->pathctrl, $this->data);
  }
  //
  public function xperiode()
  {
    $tResult = array("result" => "OK");
    try {
      $tData = $this->input->post();
      if (isset($tData["listXPERIODE"])) {
        $this->pm->Periode($tData["listXPERIODE"]);
      }
    } catch (Exception $ex) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 99;
      $tResult["desc"] = "Internal System Error";
    }
    echo json_encode($tResult, JSON_NUMERIC_CHECK);
  }
  //
  public function backxperiode()
  {
    $tResult = array("result" => "OK");
    try {
      $this->pm->ClearPeriode();
    } catch (Exception $ex) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 99;
      $tResult["desc"] = "Internal System Error (".get_class($ex).")";
    }
    echo json_encode($tResult, JSON_NUMERIC_CHECK);
  }
  //
  public function proclogin()
  {
    $tResult = array("result" => "OK");
    try {
      $this->xmdl->proses_login($this->input->post());
    } catch (Exception $ex) {
      $tResult["result"] = "ERROR";
      if (get_class($ex) === "MyException") {
        $tResult["errno"] = $ex->getCode();
        $tResult["desc"] = $ex->getMessage();
      } else {
        $tResult["errno"] = 99;
        $tResult["desc"] = "Internal System Error (".get_class($ex).")";
      }
    }
    echo json_encode($tResult, JSON_NUMERIC_CHECK);
  }
}
