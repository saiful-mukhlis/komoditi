<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Xmember
{
  public function index()
  {

    //if ($this->PageAllowed) {
      $tPageview = 'users';
      $this->load->model('susers_model', 'xmdlusers');
      //$this->data['options'] = $this->xmdlusers->list_option_skpd();
      $this->data['cssextra'][] = 'assets/jqueryui/themes/smoothness/jquery-ui.min.css';
      $this->data['cssextra'][] = 'assets/datatables/datatables/css/jquery.dataTables.min.css';
      $this->data['cssextra'][] = 'assets/datatables/datatables/css/dataTables.jqueryui.min.css';
      $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.dataTables.min.css';
      $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.jqueryui.min.css';
      $this->data['cssextra'][] = 'assets/datatables/select/css/select.dataTables.min.css';
      $this->data['cssextra'][] = 'assets/datatables/select/css/select.jqueryui.min.css';
      $this->data['jsextra'][] = 'assets/jqueryui/jquery-ui.min.js';
      $this->data['jsextra'][] = 'assets/datatables/datatables/js/jquery.dataTables.min.js';
      $this->data['jsextra'][] = 'assets/datatables/datatables/js/dataTables.jqueryui.min.js';
      $this->data['jsextra'][] = 'assets/datatables/fixedheader/js/dataTables.fixedHeader.min.js';
      $this->data['jsextra'][] = 'assets/datatables/select/js/dataTables.select.min.js';
      $this->data['jsextra'][] = 'js/users.js?_='.time();
    /*} else {
      $tPageview = 'noaccess';
      $this->data['description'] = "Anda tidak memiliki hak akses halaman ini...<br/>".
        "Silahkan click <a href='".base_url()."'>disini</a> untuk kembali ke Dashboard.";
    }*/

      $this->load->model('InputHarianSimple_model', 'inputHarian');
      $this->data['model'] = $this->inputHarian->data();


    $this->load->view('header', $this->data);
    $this->load->view($tPageview, $this->data);
    $this->load->view('footer', $this->data);
  }

    public function listdata()
    {
        ($this->IsAjax) OR exit('No direct script access allowed...');
        /////////////////////////////////////////////////////////////////
        $tResult = array("result"=>"OK");
        $this->load->model('susers_model', 'xmdl');
        $tResult = $this->xmdl->list_data();
        echo json_encode($tResult);
    }
    //
    public function tambahedit()
    {
        $tResult = array();
        $tData = $this->input->post();
//        echo '<pre>';
//        print_r($tData);
//        echo '</pre>';
//        exit;
        if ($tData['inputUid'] == '' or $tData['inputPassword'] == '') {
            $tResult['result'] = 'ERROR';
            $tResult['errno'] = '1';
            $tResult['desc'] = 'User masih kosong...';
        } else {
            $tPrefix = $tData['idedit'] == '' ? "Tambah" : "Edit";
            $this->load->model('susers_model', 'xmdl');
            if ($this->xmdl->add($tData)) {
                $tResult['result'] = 'OK';
                $tResult['desc'] = "{$tPrefix} data Pasar BERHASIL...";
            } else {
                $tResult['result'] = 'ERROR';
                $tResult['errno'] = '1';
                $tResult['desc'] = "{$tPrefix} data Pasar GAGAL...<br/>Data Pasar sudah terdaftar...";
            }
        }
        echo json_encode($tResult);
    }
    //
    public function hapus()
    {
        $tResult = array();
        $this->load->model('susers_model', 'xmdl');
        if ($this->xmdl->hapus($this->input->post())) {
            $tResult['result'] = 'OK';
            $tResult['desc'] = "Hapus data User BERHASIL...";
        } else {
            $tResult['result'] = 'ERROR';
            $tResult['errno'] = '1';
            $tResult['desc'] = "Hapus data User GAGAL...";
        }
        echo json_encode($tResult);
    }

}
