<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends Member_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('datatable');
    $this->load->model('mbank_model', 'xmdl');
  }
  /////
  public function index()
  {
    $tPageview = '';
    if ($this->PageAllowed) {
      if ($this->input->get('f')) {
        $this->pm->Clear();
        redirect(base_url('bank'));
      } else {
        $tPageview = 'bank';
        ///////////////////////////////
        $PM = $this->pm->Memory();
        if (!isset($PM['id'])) $PM['id'] = '';
        $PM = $this->pm->Memory($PM);
        ///////////////////////////////
        $this->data['cssextra'][] = 'assets/jqueryui/themes/smoothness/jquery-ui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/jquery.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/dataTables.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.jqueryui.min.css';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/jquery.dataTables.min.js';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/dataTables.jqueryui.min.js';
        $this->data['jsextra'][] = 'assets/datatables/fixedheader/js/dataTables.fixedHeader.min.js';
        $this->data['jsextra'][] = 'assets/datatables/select/js/dataTables.select.min.js';
        $this->data['jsextra'][] = 'assets/datatables/buttons/js/dataTables.buttons.min.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.numeric.extensions.js';
        $this->data['jsextra'][] = 'js/bank.js?_='.time();
      }
    } else {
      $tPageview = 'noaccess';
      $this->data['description'] = "Anda tidak memiliki hak akses halaman ini...<br/>".
        "Silahkan click <a href='".base_url()."'>disini</a> untuk kembali ke Dabankoard.";
    }
    if ($tPageview != '') {
      $this->load->view('header', $this->data);
      $this->load->view($tPageview, $this->data);
      $this->load->view('footer', $this->data);
    }
  }
  //
  public function listdata()
  {
    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $tResult = $this->xmdl->list_data();
    echo json_encode($tResult);
  }
  //
  public function updateskpd()
  {
    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $PM = $this->pm->Memory();
    $PM['skpd'] = $this->input->post('root');
    $PM['id'] = '';
    $this->pm->Memory($PM);
    echo json_encode($tResult);
  }
  //
  public function updatesaldoawal()
  {
    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $tData = $this->input->post();
    if (!empty($tData['idd'])) {
      $tData['inpsa'] = $tData['inpsa'] == '' ? '0.0' : str_replace(array('.', ','), array('', '.'), $tData['inpsa']);
      $thsl = $this->xmdl->update_saldo_awal($tData);
      if ($thsl === 1000) {
        $tResult["result"] = "ERROR";
        $tResult["errno"] = 1;
        $tResult["desc"] = "Saldo Awal sudah di Verifikasi...";
      }
    }
    echo json_encode($tResult);
  }
  //
  public function verify()
  {
    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $thsl = $this->xmdl->proc_verify();
    if ($thsl === 1000) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 1;
      $tResult["desc"] = "Saldo Awal sudah di Verifikasi...";
    } elseif ($thsl === 1100) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 2;
      $tResult["desc"] = "Saldo debet dan saldo kredit belum balance...<br/>Saldo awal belum dapat diVerifikasi...";
    } elseif ($thsl === 1200) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 2;
      $tResult["desc"] = "Saldo Awal sudah di Verifikasi...";
    } elseif ($thsl === false) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 2;
      $tResult["desc"] = "Verifikasi gagal...";
    }
    echo json_encode($tResult);
  }
  //
  public function unverify()
  {
    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $thsl = $this->xmdl->proc_unverify();
    if ($thsl === 1000) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 1;
      $tResult["desc"] = "Saldo Awal sudah di Batal Verifikasi...";
    } elseif ($thsl === false) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 2;
      $tResult["desc"] = "Batal Verifikasi gagal...";
    }
    echo json_encode($tResult);
  }
}
