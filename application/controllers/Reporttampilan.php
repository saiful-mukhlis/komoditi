<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reporttampilan extends MY_Xmember
{
  public function __construct()
{
    parent::__construct();
    $this->load->model('Report_model', 'xmdl');
}
  /////
  public function index()
  {

//      $tResult = $this->xmdl->list_data_view(date('Y-m-d') . ' 00:00:00');

      // date('Y-m-d', strtotime('-7 days'))

      
    $tPageview = '';
    //if ($this->PageAllowed) {
      if ($this->input->get('f')) {
        $this->pm->Clear();
        redirect(base_url('pasar'));
      } else {
        $tPageview = 'reporttampilan';
        ///////////////////////////////
        $PM = $this->pm->Memory();
        if (!isset($PM['id'])) $PM['id'] = '';
        $PM = $this->pm->Memory($PM);
        ///////////////////////////////
        $this->data['cssextra'][] = 'assets/jqueryui/themes/smoothness/jquery-ui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/jquery.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/dataTables.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.jqueryui.min.css';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/jquery.dataTables.min.js';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/dataTables.jqueryui.min.js';
        $this->data['jsextra'][] = 'assets/datatables/fixedheader/js/dataTables.fixedHeader.min.js';
        $this->data['jsextra'][] = 'assets/datatables/select/js/dataTables.select.min.js';
        $this->data['jsextra'][] = 'assets/datatables/buttons/js/dataTables.buttons.min.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.numeric.extensions.js';
        $this->data['jsextra'][] = 'js/report.js?_='.time();
      }


      $tgl1 = $this->xmdl->date();
      $dataNow = $this->xmdl->list_data_view($tgl1);
      $time1 = strtotime($tgl1);
      $dataYesterday = $this->xmdl->list_data_view(date('Y-m-d', strtotime('-1 days', $time1)) . ' 00:00:00');

      $dataNow2 = [];
      foreach ($dataNow as $item) {
          $dataNow2[$item->kodekomoditi] = $item;
      }
      $dataYesterday2 = [];
      foreach ($dataYesterday as $item) {
          $dataYesterday2[$item->kodekomoditi] = $item;
      }
      $this->data['model'] = [
          'date1' => date('Y-m-d', $time1),
          'date2' => date('Y-m-d', strtotime('-1 days', $time1)),
          'satuan' => $this->xmdl->satuan(),
          'dataNow' => $dataNow2,
          'dataYesterday' => $dataYesterday2,
          'title' => $this->xmdl->buildTitle(),
      ];
      

      
//      echo '<pre>';
//      print_r( $this->data['modal']);
//      echo '</pre>';
//      exit;

    /*} else {
      $tPageview = 'noaccess';
      $this->data['description'] = "Anda tidak memiliki hak akses halaman ini...<br/>".
        "Silahkan click <a href='".base_url()."'>disini</a> untuk kembali ke Dabankoard.";
    }*/
    if ($tPageview != '') {
      $this->load->view('header', $this->data);
      $this->load->view($tPageview, $this->data);
      $this->load->view('footer', $this->data);
    }
  }
  //
  public function listdata()
  {

    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $tResult = $this->xmdl->list_data();
    echo json_encode($tResult);
  }


}
