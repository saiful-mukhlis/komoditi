<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends Member_Controller
{
  public function index()
  {
    //if ($this->PageAllowed) {
      $tPageview = 'satuan';
      $this->data['jsextra'][] = 'js/satuan.js?_='.time();
    /*} else {
      $tPageview = 'noaccess';
      $this->data['description'] = "Anda tidak memiliki hak akses halaman ini...<br/>".
        "Silahkan click <a href='".base_url()."'>disini</a> untuk kembali ke Dashboard.";
    }*/
    if ($tPageview != '') {
      $this->load->view('header', $this->data);
      $this->load->view($tPageview, $this->data);
      $this->load->view('footer', $this->data);
    }
  }
}
