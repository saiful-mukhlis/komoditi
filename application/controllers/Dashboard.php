<?php
/***********************************************
 ***   NEW RECONSTRUCTION DONE - 24/10/2017  ***
 ***********************************************/
defined("BASEPATH") OR exit("No direct script access allowed");

class Dashboard extends MY_Xmember
{
  public function __construct()
  {
    parent::__construct();
  }
  //
  public function index()
  {
    //$this->data["periode"] = "(Periode Penyusunan Anggaran : {$this->data["apps"]->periode}; Periode Realisasi Anggaran : {$this->data["apps"]->periode})";
    $this->load->view("{$this->path}header", $this->data);
    $this->load->view($this->pathctrl, $this->data);
    $this->load->view("{$this->path}footer", $this->data);
  }
  //
  public function logout()
  {
    $tResult = array("result" => "OK");
    try {
      $this->session->sess_destroy();
    } catch (Exception $ex) {
      $tResult["result"] = "ERROR";
      $tResult["errno"] = 99;
      $tResult["desc"] = "Internal System Error";
    }
    echo json_encode($tResult, JSON_NUMERIC_CHECK);
  }
  //
  public function updatepassword()
  {
    $tResult = array("result" => "OK", "desc" => "Ganti Password BERHASIL...");
    try {
      $tData = $this->input->post();
      if (empty($tData["inputPasswordLama"])) throw new MyException("Password lama masih kosong...", 1);
      if (empty($tData["inputPasswordBaru"])) throw new MyException("Password baru masih kosong...", 2);
      if (empty($tData["inputKonfirmasi"])) throw new MyException("Konfirmasi masih kosong...", 3);
      if ($tData["inputKonfirmasi"] != $tData["inputPasswordBaru"]) throw new MyException("Konfirmasi tidak sama dengan Password baru...", 3);
      $this->xmdldsbrd->update_password($tData);
    } catch (Exception $ex) {
      $tResult["result"] = "ERROR";
      if (get_class($ex) === "MyException") {
        $tResult["errno"] = $ex->getCode();
        $tResult["desc"] = $ex->getMessage();
      } else {
        $tResult["errno"] = 99;
        $tResult["desc"] = "Internal System Error";
      }
    }
    echo json_encode($tResult, JSON_NUMERIC_CHECK);
  }
}
