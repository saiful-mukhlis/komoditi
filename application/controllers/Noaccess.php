<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends Member_Controller
{
  public function index()
  {
    $this->data['description'] = "Anda tidak memiliki hak akses halaman ini...<br/>Silahkan click <a href='".base_url()."'>disini</a> untuk kembali ke Dashboard.";
    $this->load->view('header', $this->data);
    $this->load->view('noaccess', $this->data);
    $this->load->view('footer', $this->data);
  }
}
