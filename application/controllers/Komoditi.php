<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komoditi extends MY_Xmember
{
  public function __construct()
  {
    parent::__construct();
    //$this->load->helper('datatable');
    $this->load->model('komoditi_model', 'xmdl');
  }
  /////
  public function index()
  {
    $tPageview = '';
    //if ($this->PageAllowed) {
      if ($this->input->get('f')) {
        $this->pm->Clear();
        redirect(base_url('komoditi'));
      } else {
        $tPageview = 'komoditi';
        ///////////////////////////////
        $PM = $this->pm->Memory();
        if (!isset($PM['id'])) $PM['id'] = '';
        $PM = $this->pm->Memory($PM);
        ///////////////////////////////
        $this->data['cssextra'][] = 'assets/jqueryui/themes/smoothness/jquery-ui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/jquery.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/dataTables.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.jqueryui.min.css';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/jquery.dataTables.min.js';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/dataTables.jqueryui.min.js';
        $this->data['jsextra'][] = 'assets/datatables/fixedheader/js/dataTables.fixedHeader.min.js';
        $this->data['jsextra'][] = 'assets/datatables/select/js/dataTables.select.min.js';
        $this->data['jsextra'][] = 'assets/datatables/buttons/js/dataTables.buttons.min.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.numeric.extensions.js';
        $this->data['jsextra'][] = 'js/komoditi.js?_='.time();
        $this->data['opsi1'] = $this->xmdl->load_option_kelompok();
        $this->data['opsi2'] = $this->xmdl->load_option_satuan();
      }
    /*} else {
      $tPageview = 'noaccess';
      $this->data['description'] = "Anda tidak memiliki hak akses halaman ini...<br/>".
        "Silahkan click <a href='".base_url()."'>disini</a> untuk kembali ke Dabankoard.";
    }*/
    if ($tPageview != '') {
      $this->load->view('header', $this->data);
      $this->load->view($tPageview, $this->data);
      $this->load->view('footer', $this->data);
    }
  }
  //
  public function listdata()
  {
    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $tResult = $this->xmdl->list_data();
      $tResult['kode'] = $this->xmdl->kode_generator();
    echo json_encode($tResult);
  }
  //
    public function tambahedit()
    {
        $tResult = array();
        $tData = $this->input->post();
        if ($tData['inputNama'] == '') {
            $tResult['result'] = 'ERROR';
            $tResult['errno'] = '1';
            $tResult['desc'] = 'Komoditi masih kosong...';
        } else {
            $tPrefix = $tData['idedit'] == '' ? "Tambah" : "Edit";
            if ($this->xmdl->tambah_edit_komoditi($tData)) {
                $tResult['result'] = 'OK';
                $tResult['desc'] = "{$tPrefix} data Komoditi BERHASIL...";
            } else {
                $tResult['result'] = 'ERROR';
                $tResult['errno'] = '1';
                $tResult['desc'] = "{$tPrefix} data Komoditi GAGAL...<br/>Data Komoditi sudah terdaftar...";
            }
        }
        echo json_encode($tResult);
    }
    //
    public function hapus()
    {
        $tResult = array();
        if ($this->xmdl->hapus_komoditi($this->input->post())) {
            $tResult['result'] = 'OK';
            $tResult['desc'] = "Hapus data Komoditi BERHASIL...";
        } else {
            $tResult['result'] = 'ERROR';
            $tResult['errno'] = '1';
            $tResult['desc'] = "Hapus data Komoditi GAGAL...";
        }
        echo json_encode($tResult);
    }



}
