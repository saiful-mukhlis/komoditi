<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class Satuan extends Ajaxmember_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('msatuan_model', 'xmdlsatuan');
  }
  //
  public function index()
  {
    $tResult = array();
    $tDump['listing'] = $this->xmdlsatuan->load_list_satuan();
    $tResult['listing'] = $this->load->view('childs/list_satuan', $tDump, TRUE);
    $tResult['result'] = 'OK';
    echo json_encode($tResult);
  }
  //
  public function tambahedit()
  {
    $tResult = array();
    $tData = $this->input->post();
    if ($tData['inputSatuan'] == '') {
      $tResult['result'] = 'ERROR';
      $tResult['errno'] = '1';
      $tResult['desc'] = 'Satuan masih kosong...';
    } else {
      $tPrefix = $tData['inputOldSatuan'] == '' ? "Tambah" : "Edit";
      if ($this->xmdlsatuan->tambah_edit_satuan($tData)) {
        $tResult['result'] = 'OK';
        $tResult['desc'] = "{$tPrefix} data Satuan BERHASIL...";
      } else {
        $tResult['result'] = 'ERROR';
        $tResult['errno'] = '1';
        $tResult['desc'] = "{$tPrefix} data Satuan GAGAL...<br/>Data satuan sudah terdaftar...";
      }
    }
    echo json_encode($tResult);
  }
  //
  public function hapus()
  {
    $tResult = array();
    if ($this->xmdlsatuan->hapus_satuan($this->input->post())) {
      $tResult['result'] = 'OK';
      $tResult['desc'] = "Hapus data Satuan BERHASIL...";
    } else {
      $tResult['result'] = 'ERROR';
      $tResult['errno'] = '1';
      $tResult['desc'] = "Hapus data Satuan GAGAL...";
    }
    echo json_encode($tResult);
  }
}
