<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class Users extends Ajaxmember_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('susers_model', 'xmdlusers');
  }
  //
  public function index()
  {
    $tResult = array();
    $tResult['data'] = $this->xmdlusers->load_list_users();
    //$tResult['listing'] = $this->load->view('childs/list_users', $tDump, TRUE);
    $tResult['result'] = 'OK';
    echo json_encode($tResult);
  }
  //
  public function tambahedit()
  {
    $tResult = array();
    $tData = $this->input->post();
    if ($tData['inputUID'] == '') {
      $tResult['result'] = 'ERROR';
      $tResult['errno'] = '1';
      $tResult['desc'] = 'UID masih kosong...' . print_r($tData, true);
    } else if ($tData['inputNama'] == '') {
      $tResult['result'] = 'ERROR';
      $tResult['errno'] = '1';
      $tResult['desc'] = 'Nama masih kosong...';
    } else {
      if ($tTemp = $this->xmdlusers->tambah_edit_users($tData)) {
        if ($tData['inputKode'] == '') $tKet = "Tambah data User {$tData['inputUID']} BERHASIL...<br/>Password : <strong>{$tTemp['password']}</strong>";
        else $tKet = "Update data User BERHASIL...";
        $tResult['result'] = 'OK';
        $tResult['desc'] = $tKet;
      } else {
        if ($tData['inputKode'] == '') $tKetE = "Tambah data User GAGAL...<br/>Data user sudah terdaftar...";
        else $tKetE = "Update data User GAGAL...<br/>Data user sudah terdaftar...";
        $tResult['result'] = 'ERROR';
        $tResult['errno'] = '1';
        $tResult['desc'] = $tKetE;
      }
    }
    echo json_encode($tResult);
  }
  //
  public function hapus()
  {
    $tResult = array();
    if ($this->xmdlusers->hapus_users($this->input->post('kode'))) {
      $tResult['result'] = 'OK';
      $tResult['desc'] = "Hapus data User BERHASIL...";
    } else {
      $tResult['result'] = 'ERROR';
      $tResult['errno'] = '1';
      $tResult['desc'] = "Hapus data User GAGAL...";
    }
    echo json_encode($tResult);
  }
  //
  public function resetpassword()
  {
    $tResult = array();
    if ($tTemp = $this->xmdlusers->reset_password_users($this->input->post('kode'))) {
      $tResult['result'] = 'OK';
      $tResult['desc'] = "Generate Password User BERHASIL...<br/>Password Baru : <strong>{$tTemp['password']}</strong>";
    } else {
      $tResult['result'] = 'ERROR';
      $tResult['errno'] = '1';
      $tResult['desc'] = "Generate Password User GAGAL...";
    }
    echo json_encode($tResult);
  }
}
