<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends Member_Controller
{
  public function index()
  {
    $this->load->view('header', $this->data);
    $this->load->view('notfound', $this->data);
    $this->load->view('footer', $this->data);
  }
}
