<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends MY_Xmember
{
  public function __construct()
  {
    parent::__construct();
    //$this->load->helper('datatable');
    $this->load->model('satuan_model', 'xmdl');
  }
  /////
  public function index()
  {
    $tPageview = '';
    //if ($this->PageAllowed) {
      if ($this->input->get('f')) {
        $this->pm->Clear();
        redirect(base_url('satuan'));
      } else {
        $tPageview = 'satuan';
        ///////////////////////////////
        $PM = $this->pm->Memory();
        if (!isset($PM['id'])) $PM['id'] = '';
        $PM = $this->pm->Memory($PM);
        ///////////////////////////////
        $this->data['cssextra'][] = 'assets/jqueryui/themes/smoothness/jquery-ui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/jquery.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/dataTables.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.jqueryui.min.css';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/jquery.dataTables.min.js';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/dataTables.jqueryui.min.js';
        $this->data['jsextra'][] = 'assets/datatables/fixedheader/js/dataTables.fixedHeader.min.js';
        $this->data['jsextra'][] = 'assets/datatables/select/js/dataTables.select.min.js';
        $this->data['jsextra'][] = 'assets/datatables/buttons/js/dataTables.buttons.min.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.numeric.extensions.js';
        $this->data['jsextra'][] = 'js/satuan.js?_='.time();
      }
    /*} else {
      $tPageview = 'noaccess';
      $this->data['description'] = "Anda tidak memiliki hak akses halaman ini...<br/>".
        "Silahkan click <a href='".base_url()."'>disini</a> untuk kembali ke Dabankoard.";
    }*/
    if ($tPageview != '') {
      $this->load->view('header', $this->data);
      $this->load->view($tPageview, $this->data);
      $this->load->view('footer', $this->data);
    }
  }
  //
  public function listdata()
  {
    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $tResult = $this->xmdl->list_data();
      $tResult['kode'] = $this->xmdl->kode_generator();
    echo json_encode($tResult);
  }
  //
    public function tambahedit()
    {
        $tResult = array();
        $tData = $this->input->post();
        if ($tData['inputSatuan'] == '') {
            $tResult['result'] = 'ERROR';
            $tResult['errno'] = '1';
            $tResult['desc'] = 'Satuan masih kosong...';
        } else {
            $tPrefix = $tData['idedit'] == '' ? "Tambah" : "Edit";
            if ($this->xmdl->tambah_edit_satuan($tData)) {
                $tResult['result'] = 'OK';
                $tResult['desc'] = "{$tPrefix} data Satuan BERHASIL...";
            } else {
                $tResult['result'] = 'ERROR';
                $tResult['errno'] = '1';
                $tResult['desc'] = "{$tPrefix} data Satuan GAGAL...<br/>Data Satuan sudah terdaftar...";
            }
        }
        echo json_encode($tResult);
    }
    //
    public function hapus()
    {
        $tResult = array();
        if ($this->xmdl->hapus_satuan($this->input->post())) {
            $tResult['result'] = 'OK';
            $tResult['desc'] = "Hapus data Satuan BERHASIL...";
        } else {
            $tResult['result'] = 'ERROR';
            $tResult['errno'] = '1';
            $tResult['desc'] = "Hapus data Satuan GAGAL...";
        }
        echo json_encode($tResult);
    }



}
