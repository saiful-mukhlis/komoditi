<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inputharian extends MY_Xmember
{
  public function __construct()
  {
    parent::__construct();
    //$this->load->helper('datatable');

    
  }
  /////
  public function index()
  {

      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
          $this->load->model('InputHarianSimple_model', 'inputHarian');
          $this->inputHarian->save($_POST);
          exit;
      }

    $tPageview = '';
    //if ($this->PageAllowed) {
      if ($this->input->get('f')) {
        $this->pm->Clear();
        redirect(base_url('inputharian'));
      } else {
        $tPageview = 'Inputharian';
        ///////////////////////////////
        $PM = $this->pm->Memory();
        if (!isset($PM['id'])) $PM['id'] = '';
        $PM = $this->pm->Memory($PM);
        ///////////////////////////////
        $this->data['cssextra'][] = 'assets/jqueryui/themes/smoothness/jquery-ui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/jquery.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/datatables/css/dataTables.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/fixedheader/css/fixedHeader.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.dataTables.min.css';
        $this->data['cssextra'][] = 'assets/datatables/select/css/select.jqueryui.min.css';
        $this->data['cssextra'][] = 'assets/datepicker/datepicker3.css';
        $this->data['jsextra'][] = 'assets/datepicker/bootstrap-datepicker.js';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/jquery.dataTables.min.js';
        $this->data['jsextra'][] = 'assets/datatables/datatables/js/dataTables.jqueryui.min.js';
        $this->data['jsextra'][] = 'assets/datatables/fixedheader/js/dataTables.fixedHeader.min.js';
        $this->data['jsextra'][] = 'assets/datatables/select/js/dataTables.select.min.js';
        $this->data['jsextra'][] = 'assets/datatables/buttons/js/dataTables.buttons.min.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.js';
        $this->data['jsextra'][] = 'assets/input-mask/jquery.inputmask.numeric.extensions.js';
        $this->data['jsextra'][] = 'js/pasar.js?_='.time();
      }
    /*} else {
      $tPageview = 'noaccess';
      $this->data['description'] = "Anda tidak memiliki hak akses halaman ini...<br/>".
        "Silahkan click <a href='".base_url()."'>disini</a> untuk kembali ke Dabankoard.";
    }*/
    if ($tPageview != '') {
        $this->load->model('InputHarianSimple_model', 'inputHarian');
        $this->data['model'] = $this->inputHarian->data();

      $this->load->view('header', $this->data);
      $this->load->view($tPageview, $this->data);
      $this->load->view('footer', $this->data);

    }
  }
  //
  public function listdata()
  {
    ($this->IsAjax) OR exit('No direct script access allowed...');
    /////////////////////////////////////////////////////////////////
    $tResult = array("result"=>"OK");
    $tResult = $this->xmdl->list_data();
      $tResult['kode'] = $this->xmdl->kode_generator();
    echo json_encode($tResult);
  }
  //
    public function tambahedit()
    {
        $tResult = array();
        $tData = $this->input->post();
        if ($tData['inputNama'] == '' or $tData['inputAlamat'] == '') {
            $tResult['result'] = 'ERROR';
            $tResult['errno'] = '1';
            $tResult['desc'] = 'Pasar masih kosong...';
        } else {
            $tPrefix = $tData['idedit'] == '' ? "Tambah" : "Edit";
            if ($this->xmdl->tambah_edit_pasar($tData)) {
                $tResult['result'] = 'OK';
                $tResult['desc'] = "{$tPrefix} data Pasar BERHASIL...";
            } else {
                $tResult['result'] = 'ERROR';
                $tResult['errno'] = '1';
                $tResult['desc'] = "{$tPrefix} data Pasar GAGAL...<br/>Data Pasar sudah terdaftar...";
            }
        }
        echo json_encode($tResult);
    }
    //
    public function hapus()
    {
        $tResult = array();
        if ($this->xmdl->hapus_pasar($this->input->post())) {
            $tResult['result'] = 'OK';
            $tResult['desc'] = "Hapus data Pasar BERHASIL...";
        } else {
            $tResult['result'] = 'ERROR';
            $tResult['errno'] = '1';
            $tResult['desc'] = "Hapus data Pasar GAGAL...";
        }
        echo json_encode($tResult);
    }



}
