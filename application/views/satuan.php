<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Satuan</h1>
  <ol class="breadcrumb">
    <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Data Master</a></li>
    <li class="active">Satuan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  
  <!-- List / Search SPP -->
  <div id="frontPage" class="box">
    <div class="box-body">
      <table id="listdata" class="display compact" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Kode</th>
            <th>Satuan</th>
            <th>Kontrol</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
  <!-- /.box -->
  
  <!-- Edit Bank-->
  <div class="modal xtambah-edit-satuan">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Satuan</h4>
        </div>
        <form class="form-horizontal" id="formTambahEdit">
          <input type="hidden" id="idedit" name="idedit"/>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="inputKode" class="col-sm-2 control-label">Kode</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="inputKode" name="inputKode" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputSatuan" class="col-sm-2 control-label">Satuan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" id="inputSatuan" name="inputSatuan">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-sm btn-success btn-flat">Simpan</button>
            <button type="button" class="btn btn-sm btn-default btn-flat" data-dismiss="modal">Batal</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
</section>
<!-- /.content -->
