<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Pasar</h1>
  <ol class="breadcrumb">
    <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Transaksi Pasar</a></li>
    <li class="active">Input Harga Harian Komoditi</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  
  <!-- List / Search SPP -->
  <div id="frontPage" class="box">
    <div class="box-body">
      <table id="listdata" class="display compact" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Kode</th>
            <th>Nama</th>
            <th>Satuan</th>
            <th>Harga</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
  <!-- /.box -->
  
  <!-- Edit Bank-->
  <div class="modal xtambah-edit-pasar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Edit Harga Komoditi</h4>
        </div>
        <form class="form-horizontal" id="formTambahEdit">
          <input type="hidden" id="idedit" name="idedit"/>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                  <label for="inputKode" class="col-sm-2 control-label">Kode</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control input-sm" id="inputKode" name="inputKode" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputNama" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" id="inputNama" name="inputNama" readonly>
                  </div>
                </div>
                  <div class="form-group">
                      <label for="inputAlamat" class="col-sm-2 control-label">Satuan</label>
                      <div class="col-sm-10">
                          <input type="text" class="form-control input-sm" id="inputSatuan" name="inputSatuan" readonly>
                      </div>
                  </div>
                <div class="form-group">
                  <label for="inputNama" class="col-sm-2 control-label">Harga</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" id="inputHarga" name="inputHarga">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-sm btn-success btn-flat">Simpan</button>
            <button type="button" class="btn btn-sm btn-default btn-flat" data-dismiss="modal">Batal</button>
          </div>
        </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  
</section>
<!-- /.content -->
