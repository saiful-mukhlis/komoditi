<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Report</h1>
  <ol class="breadcrumb">
    <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Report</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

    <div id="wrapper">
        <div id="header">
            <div id="logo"><img src="/assets/images/jatim-bw.jpg" width="60"></div>
            <div id="title">
        <span style="font-weight: bold">
        <span style="font-size: 12pt">DINAS PERINDUSTRIAN DAN PERDAGANGAN</span> <br>
        PROVINSI JAWA TIMUR <br>
        JL. SIWALANKERTO UTARA II/42 PO.BOX.4110  <br>
        </span>
                Telepon :  (031) 8421140 - 8470227 - 8434047 - 8434749 - 8432373  <br>
                SURABAYA
            </div>
            <br style="clear:both;">
        </div>
        <div id="body">
            <div id="to">
                Kepada<br>
                Yth. Bpk. Dirjen Perdagangan Dalam Negri Kemendag RI      <br>
                di<br>
                &nbsp;&nbsp;&nbsp;<u>TEMPAT</u>
            </div>
            <br style="clear: both">
            <div id="subttl">
                DAFTAR ISIAN HARGA RATA-RATA BEBERAPA BAHAN POKOK PANGAN <br>
                DI PASAR DINOYO
            </div>

            <style>
                td, tr{
                    border:1px solid #000;
                }
            </style>


            <div class="pasar">Tanggal Pengamatan : 06.12.2017  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pasar : </div>
            <table class="grid">
                <tbody><tr>
                    <td rowspan="2"><div align="center">No</div></td>
                    <td rowspan="2"><div align="center">Nama Bahan Pokok<br>
                            dan Jenisnya </div></td>
                    <td rowspan="2"><div align="center">Satuan</div></td>
                    <td colspan="2"><div align="center">Harga</div></td>
                    <td colspan="2"><div align="center">Stok</div></td>
                    <td colspan="2"><div align="center">Perubahan</div></td>
                    <td rowspan="2"><div align="center" style="width:3cm">Ket</div></td>
                    <td rowspan="2"><div align="center">Perubahan /<br>
                            Kondisi<br> saat ini </div></td>
                </tr>
                <tr>
                    <td><div align="center">Kemarin<br> <?php echo $model['date1'] ?> </div></td>
                    <td><div align="center">Hari ini<br><?php echo $model['date2'] ?> </div></td>
                    <td><div align="center">Kemarin<br><?php echo $model['date1'] ?></div></td>
                    <td><div align="center">Hari ini<br><?php echo $model['date2'] ?> </div></td>
                    <td><div align="center" style="width:1.5cm">Rp</div></td>
                    <td><div align="center" style="width:1.5cm">%</div></td>
                </tr>

                <?php
                foreach ($model['title'] as $title) {
                    ?>
                    <tr>
                        <td><?php echo $title->kode ?></td>
                        <td>
                            <?php echo $title->nama ?>          </td>
                        <td></td>
                        <td class="right"></td>
                        <td class="right"></td>
                        <td class="right"></td>
                        <td class="right"> <span class=""></span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                <?php

                    foreach ($title->child as $key => $item) {
                        ?>
                        <tr>
                            <td></td>
                            <td>
                                - <?php echo $item->nama ?>          </td>

                            <td class="right"> <?php echo $model['satuan'][$item->kodesatuan]->nama ?> </td>
                            <td class="right"> <?php if(!empty($model['dataNow'][$item->kode])) echo $a = $model['dataNow'][$item->kode]->harga ?> </td>
                            <td class="right"> <?php if(!empty($model['dataYesterday'][$item->kode])) echo $b = $model['dataYesterday'][$item->kode]->harga ?> </td>

                            <td class="right"> <?php if(!empty($model['dataNow'][$item->kode])) echo $c = $model['dataNow'][$item->kode]->stok ?> </td>
                            <td class="right"> <?php if(!empty($model['dataYesterday'][$item->kode])) echo $d = $model['dataYesterday'][$item->kode]->stok ?> </td>
                            <td class="right"> <span class="">
                                    <?php
                                    if(!empty($a) && !empty($b)){
                                        echo $s = $a-$b;
                                    }else{
                                        if(!empty($a)){
                                            echo $s = $a;
                                        }else if(!empty($b)){
                                            echo $s = $b;
                                        }else{
                                            $s=0;
                                        }
                                    }
                                    ?>
                                </span></td>
                            <td>
                                <?php
                                    if(!empty($b)){
                                        $p = $b;
                                    }else if(!empty($a)){
                                        $p = $a;
                                    }else{
                                        $p = 0;
                                    }
                                    if(!empty($p)){
                                        echo round($s/$p*100);
                                    }
                                ?>
                            </td>
                            <td></td>
                            <td></td>

                        </tr>
                        <?php
                    }

                }
                ?>





                </tbody></table>    <br><br>

            <table>
                <tbody><tr>
                    <td valign="top">Tembusan<br>Kepada Yth :</td>
                    <td style="width:10cm"> <br>
                        1. Bpk. Gubernur Jawa Timur<br>
                        2. Sdr. Dir. Bapoksta Kemendag RI<br>
                        3. Bpk. Sekda Prop Jatim<br>
                        4. Sdr. Asistem II prop Jatim<br>
                        5. Sdr. Kepala Dinas Kominfo prop Jatim<br>
                        6. Sdr. Dinas Pertanian prop Jatim<br>
                        7. Sdr. Kepala BKP prop Jatim<br>
                        8. Sdr. Kepala Dinas Perikanan prop Jatim<br>
                        9. Sdr. Kepala Dinas Peternakan prop Jatim<br>
                        10. Sdr. Kepala Dinas Perkebunan Prop Jatim<br>
                        11. Sdr. Kepala RRI Stasiun Surabaya<br>
                        12. sdr. PimpinanRedaksi Harian Radar Surabaya              </td>
                    <td style="text-align: center">
                        a/n. KEPALA DINAS <br>PERINDUSTRIAN DAN PERDAGANGAN <br>PROVINSI JAWA TIMUR<br>
                        KEPALA BIDANG <br>PERDAGANGAN DALAM NEGERI<br>
                        <br><br><br><br><br>
                        <u>Ir.  S A I F U L   J A S A N   </u><br>
                        Pembina Tk. I    <br>
                        NIP. 19621127 199003 1 009              </td>
                </tr>
                </tbody></table>
        </div>
    </div>

</section>
<!-- /.content -->
