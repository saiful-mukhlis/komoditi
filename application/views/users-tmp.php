<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>User</h1>
  <ol class="breadcrumb">
    <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Setting</a></li>
    <li class="active">User</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  
  <!-- List Users -->
  <div id="frontPage" class="box">
    <div class="box-body">
      <table id="listusers" class="display compact" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>UID</th>
            <th>Nama</th>
            <th>SKPD</th>
            <th>Kontrol</th>
          </tr>
        </thead>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  
  <!-- Form User  -->
  <div id="pageMasterDetail" class="box hidden">
    <div class="box-header with-border">
      <button id="tombolKembali" class="btn btn-sm bg-orange btn-flat"><i class="fa fa-mail-reply"></i> Kembali</button>
      <h3 class="box-title"></h3>
    </div>
    <form id="formUser" class="form-horizontal">
      <input type="hidden" id="inputKode" name="inputKode">
      <input type="hidden" id="inputHakAkses" name="inputHakAkses">
      <div class="box-body">
        <div class="col-sm-8">
          <div class="form-group">
            <label for="inputUID" class="col-sm-4 control-label">UID</label>
            <div class="col-sm-8">
              <input type="text" class="form-control input-sm" id="inputUID" name="inputUID">
            </div>
          </div>
          <div class="form-group">
            <label for="inputNama" class="col-sm-4 control-label">Nama</label>
            <div class="col-sm-8">
              <input type="text" class="form-control input-sm" id="inputNama" name="inputNama">
            </div>
          </div>
          <div class="form-group">
            <label for="inputSKPD" class="col-sm-4 control-label">SKPD</label>
            <div class="col-sm-8">
              <select class="form-control input-sm" name="inputSKPD" id="inputSKPD">
                <option value=""></option>
                <option value="all">All</option>
                <? foreach($options as $row): ?>
                  <option value="<?=$row->KodeSatker?>"><?=$row->KodeSatker." - ".$row->Nama?></option>
                <? endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="inputHak" class="col-sm-4 control-label">Hak Akses</label>
            
            <div class="col-sm-8">
              <div class="box box-solid">
                <div class="box-body">
                  <div class="box-group" id="usersetting">
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-primary">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupMaster">
                            UMUM - Data Master
                          </a>
                        </h4>
                      </div>
                      <div id="groupMaster" class="panel-collapse collapse in">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m000"> Rekening - Rekening Permen 13</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m001"> Rekening - Rekening Permen 64</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m002"> SKPD - SKPD</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m056"> SKPD - UPTD</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m073"> SKPD - Bagian</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m003"> Program dan Kegiatan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m004"> Satuan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m005"> Tim Anggaran</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m087"> Bank</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m022"> Pihak Ketiga</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-primary">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupSetting">
                            UMUM - Setting
                          </a>
                        </h4>
                      </div>
                      <div id="groupSetting" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m006"> Periode</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m023"> Setting Aplikasi</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m007"> User</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m008"> Perangkat Daerah</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m009"> User Kegiatan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m046"> Pelampauan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m076"> Akuntansi - Mapping Rek. 13 ke 64</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m077"> Akuntansi - Mapping LRA ke LO</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m078"> Akuntansi - Mapping Rekening Denda</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m079"> Akuntansi - Mapping Rekening Piutang</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m080"> Akuntansi - Mapping Rekening Hutang</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m089"> Akuntansi - Mapping Klasifikasi BO</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m088"> Setting Online Payment</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-success">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupPenyusunan">
                            ANGGARAN - Penyusunan Anggaran
                          </a>
                        </h4>
                      </div>
                      <div id="groupPenyusunan" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m010"> Pagu Indikatif</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m064"> RKBMD - RKBMD</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m069"> RKBMD - Verifikasi RKBMD</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m071"> RKBMD - Verifikasi RKBMD Pusat</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m011"> Penyusunan (RKA)</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m013"> Penetapan (DPA) - DPA Kegiatan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m014"> Penetapan (DPA) - DPA Global</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m015"> Penetapan (DPA) - Perangkat Penatausahaan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m016"> Perubahan Perbup</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-success">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupPerubahan">
                            ANGGARAN - Perubahan Anggaran
                          </a>
                        </h4>
                      </div>
                      <div id="groupPerubahan" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m017"> Pagu Indikatif Perubahan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m065"> RKBMDP - RKBMDP</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m070"> RKBMDP - Verifikasi RKBMDP</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m072"> RKBMDP - Verifikasi RKBMDP Pusat</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m018"> Perubahan (PAK)</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m019"> Penetapan Perubahan (DPPA) - DPPA Kegiatan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m020"> Penetapan Perubahan (DPPA) - DPPA Global</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m021"> Perubahan Perbup (PAK)</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-success">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupLaporanAng">
                            ANGGARAN - Laporan
                          </a>
                        </h4>
                      </div>
                      <div id="groupLaporanAng" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m012"> Laporan - Blanko RKA / DPA</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m060"> Laporan - Lampiran APBD</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m075"> Laporan - Blanko RKA / DPA Perubahan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m061"> Laporan - Lampiran Perubahan APBD</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupPendapatan">
                            PENATAUSAHAAN - Pendapatan
                          </a>
                        </h4>
                      </div>
                      <div id="groupPendapatan" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m024"> Surat Ketetapan Pajak</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m025"> Tanda Bukti Pembayaran</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m026"> Slip Setoran</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m027"> Surat Tanda Setoran</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m047"> Rekap Pendapatan</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupBelanja">
                            PENATAUSAHAAN - Belanja
                          </a>
                        </h4>
                      </div>
                      <div id="groupBelanja" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m028"> Surat Penyediaan Dana</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m029"> Kontrak Pekerjaan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m030"> Serah Terima Pekerjaan</label>
                          </div>
                          <div class="checkbox">
                           <label><input type="checkbox" id="m031"> SPP - SPP UP</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m032"> SPP - SPP GU</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m033"> SPP - SPP TU</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m034"> SPP - SPP LS</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m035"> SPP - Verifikasi</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m036"> SPM - SPM</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m037"> SPM - Verifikasi</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m038"> SP2D - SP2D</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m039"> SP2D - Pencairan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m048"> Pergeseran - Pergeseran Kas</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m040"> Pergeseran - NPD</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m041"> Pergeseran - NPK</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m074"> Pergeseran - Pindah Buku</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m042"> Nota Panjar</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m043"> SPJ - SPJ Panjar</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m062"> SPJ - Verifikasi PPK SKPD</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m053"> SPJ - Pengembalian Sisa Panjar</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m045"> SPJ - SPJ TU</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m054"> SPJ - Pengembalian Sisa TU</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m049"> SPJ - Setoran Pajak</label>
                          </div>
                          
                          <div class="checkbox">
                            <label><input type="checkbox" id="m050"> Contra Post - SP2D LS</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m051"> Contra Post - SPJ Panjar</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m052"> Contra Post - SPJ TU</label>
                          </div>
                          
                          <div class="checkbox">
                            <label><input type="checkbox" id="m057"> SP Pendapatan & Belanja - Pengesahan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m058"> SP Pendapatan & Belanja - Verifikasi Pengesahan</label>
                          </div>
                          
                          <div class="checkbox">
                            <label><input type="checkbox" id="m090"> PPB - PPB</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m091"> PPB - Verifikasi PPB</label>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupKoreksi">
                            PENATAUSAHAAN - Koreksi
                          </a>
                        </h4>
                      </div>
                      <div id="groupKoreksi" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m066"> Koreksi</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupLaporanPTU">
                            PENATAUSAHAAN - Laporan
                          </a>
                        </h4>
                      </div>
                      <div id="groupLaporanPTU" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m067"> Laporan - Pendapatan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m068"> Laporan - Belanja</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m044"> Laporan - Penatausahaan</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m059"> Laporan - Administrasi Pembangunan</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-danger">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupSaldoAwal">
                            AKUNTANSI - Saldo Awal
                          </a>
                        </h4>
                      </div>
                      <div id="groupSaldoAwal" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m081"> Saldo Awal</label>
                          </div>
                          <!--div class="checkbox">
                            <label><input type="checkbox" id="m082"> Saldo Awal PPKD</label>
                          </div -->
                        </div>
                      </div>
                    </div>

                    <div class="panel box box-danger">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupJurnal">
                            AKUNTANSI - Jurnal
                          </a>
                        </h4>
                      </div>
                      <div id="groupJurnal" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m083"> Jurnal - Jurnal Umum</label>
                          </div>
                          <div class="checkbox">
                            <label><input type="checkbox" id="m084"> Jurnal - Jurnal Penyesuaian</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    
                    <div class="panel box box-danger">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#usersetting" href="#groupAkLaporan">
                            AKUNTANSI - Laporan
                          </a>
                        </h4>
                      </div>
                      <div id="groupAkLaporan" class="panel-collapse collapse">
                        <div class="box-body">
                          <div class="checkbox">
                            <label><input type="checkbox" id="m085"> Laporan</label>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            
            
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
        &nbsp;&nbsp;
        <button type="button" class="btn btn-default btn-sm xbatal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
      </div>
      <!-- /.box-footer-->
    </form>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
  
</section>
<!-- /.content -->
