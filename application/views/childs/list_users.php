<?php
defined('BASEPATH') OR exit('No direct script access allowed');

foreach($listing as $row):
?>
  <tr data-kode="<?=$row->Kode?>" data-hak="<?=$row->Hak?>" data-satker="<?=$row->KodeSatker?>">
    <td><?=$row->UID?></td>
    <td><?=$row->Nama?></td>
    <td><?=$row->KodeSatker.($row->Satker == '' ? '' : " - ".$row->Satker)?></td>
    <td class="text-right">
      <button class="btn btn-sm btn-primary xedit"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</button>&nbsp;&nbsp;
      <button class="btn btn-sm btn-warning xreset"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Generate Password</button>&nbsp;&nbsp;
      <button class="btn btn-sm btn-danger xdelete"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Hapus</button>
    </td>
  </tr>
<?php endforeach; ?>
