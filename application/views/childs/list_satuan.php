<?php
defined('BASEPATH') OR exit('No direct script access allowed');

foreach ($listing as $row) :
?>
  <tr>
    <td><?=$row->satuan?></td>
    <td class="text-right">
      <button class="btn btn-sm btn-primary xedit"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</button>
      &nbsp;&nbsp;<button class="btn btn-sm btn-danger xdelete"><i class="fa fa-eraser"></i>&nbsp;&nbsp;Hapus</button>
    </td>
  </tr>
<?php endforeach; ?>
