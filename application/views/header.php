<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Komoditi Bahan Pokok</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- BEGIN CSS -->
    <?=CSSList()?>
    <!-- END CSS -->
    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?= base_url('favicon.ico') ?>">
  </head>
  <body class="hold-transition skin-purple fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="<?=base_url()?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">SIKBP</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">SIKBP </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="images/user.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?= $user->nama ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="images/user.jpg" class="img-circle" alt="User Image">
                    <p><?= $user->nama ?></p>
                    <button class="btn btn-primary btn-flat btn-sm" id="xLogout"><i class="fa fa-sign-out" aria-hidden="true"></i> Log out</button>&nbsp;&nbsp;
                    <button class="btn bg-maroon btn-flat btn-sm" id="xChPassword"><i class="fa fa-key" aria-hidden="true"></i> Ganti Password</button>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->

        <?php
        if($u_hak == 'admin'){
        ?>
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="images/user.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?= $user->nama ?></p>
                            <a href="#"><i class="fa fa-circle text-success" aria-hidden="true"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">

                        <!--? if ($this->am->uac('m000,m001,m002,m003,m004,m005,m022,m056,m006,m023,m007,m008,m009,m046')): ?-->
                        <li class="header">DATA MASTER</li>
                        <!-- ? endif; ?> -->


                        <!-- ? if ($this->am->uac('m000,m001,m002,m003,m004,m005,m022,m056,m087')): ?-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-database" aria-hidden="true"></i> <span>Data Master</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i>
                            </a>
                            <ul class="treeview-menu">

                                <!--? if ($this->am->uac('m000')): ?-->
                                <li><a href="<?= base_url('pasar?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Pasar</a></li>
                                <!--? endif; ?-->

                                <!-- ? if ($this->am->uac('m002,m056,m073')): ?-->
                                <li>
                                    <a href="#"><i class="fa fa-circle-o" aria-hidden="true"></i> Referensi<i class="fa fa-angle-left pull-right" aria-hidden="true"></i></a>
                                    <ul class="treeview-menu">
                                        <!-- ? if ($this->am->uac('m002:all')): ? -->
                                        <li><a href="<?= base_url('satuan?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Satuan</a></li>
                                        <!-- ? endif; ? -->
                                        <!-- ? if ($this->am->uac('m056')): ? -->
                                        <li><a href="<?= base_url('kelompok?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Kelompok</a></li>
                                        <!-- ? endif; ? -->
                                    </ul>
                                </li>
                                <!-- ? endif; ? -->


                                <!--? if ($this->am->uac('m003:all')): ? -->
                                <li><a href="<?= base_url('komoditi?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Komoditi</a></li>
                                <!-- ? endif; ?-->
                            </ul>
                        </li>
                        <!--? endif; ?-->

                        <!--? if ($this->am->uac('m006,m023,m007,m008,m009,m046,m088')): ?-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-gears" aria-hidden="true"></i> <span>Setting</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i>
                            </a>
                            <ul class="treeview-menu">
                                <!--? if ($this->am->uac('m007:all')): ?-->
                                <li><a href="<?= base_url('users') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> User</a></li>
                                <!--? endif; ?-->

                            </ul>
                        </li>
                        <!--? endif; ?-->

                        <!--? if ($this->am->uac('m010,m064,m011,m013,m014,m015,m016,m017,m065,m018,m019,m020,m021,m012,m060,m061')): ?-->
                        <li class="header">TRANSAKSI</li>
                        <!--? endif; ?-->
                        <!--? if ($this->am->uac('m010,m064,m011,m013,m014,m015,m016')): ?-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-money" aria-hidden="true"></i> <span>Pasar</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i>
                            </a>
                            <ul class="treeview-menu">
                                <!--? if ($this->am->uac('m010')): ?-->
                                <li><a href="<?= base_url('inputharian?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Input Harian Harga Komoditi</a></li>
                                <!--? endif; ?-->
                            </ul>
                        </li>
                        <!--? endif; ?-->
                        <!--? if ($this->am->uac('m017,m065,m018,m019,m020,m021')): ?-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-money" aria-hidden="true"></i> <span>Verifikator</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i>
                            </a>

                            <ul class="treeview-menu">
                                <!--? if ($this->am->uac('m017')): ?-->
                                <li><a href="<?= base_url('verifikasiharga?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Verifikasi Harga Komoditi</a></li>
                                <!--? endif; ?-->

                                <!--? if ($this->am->uac('m018')): ?-->
                                <li><a href="<?= base_url('publishharga?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Publish Harga Komoditi</a></li>
                                <!--? endif; ?-->
                            </ul>
                        </li>
                        <!--? endif; ?-->

                        <!--? if ($this->am->uac('m010,m064,m011,m013,m014,m015,m016,m017,m065,m018,m019,m020,m021,m012,m060,m061')): ?-->
                        <li class="header">LAPORAN</li>
                        <!--? endif; ?-->

                        <!--? if ($this->am->uac('m012,m060,m061,m075')): ?-->
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i>
                            </a>
                            <ul class="treeview-menu">
                                <!--? if ($this->am->uac('m012')): ?-->
                                <li><a href="<?= base_url('blankorka?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Pasar</a></li>
                                <!--? endif; ?-->
                                <!--? if ($this->am->uac('m060')): ?-->
                                <li><a href="<?= base_url('lampiranapbd?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Komoditi</a></li>
                                <!--? endif; ?-->
                                <!--? if ($this->am->uac('m075')): ?-->
                                <li><a href="<?= base_url('blankorkap?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Harga Komoditi</a></li>
                                <!--? endif; ?-->
                            </ul>
                        </li>
                        <!--? endif; ?-->

                </section>
                <!-- /.sidebar -->
            </aside>
        <?php
        }else{
            ?>
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="images/user.jpg" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?= $user->nama ?></p>
                            <a href="#"><i class="fa fa-circle text-success" aria-hidden="true"></i> Online</a>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-money" aria-hidden="true"></i> <span>Pasar</span> <i class="fa fa-angle-left pull-right" aria-hidden="true"></i>
                            </a>
                            <ul class="treeview-menu">
                                <!--? if ($this->am->uac('m010')): ?-->
                                <li><a href="<?= base_url('inputharian?f=1') ?>"><i class="fa fa-circle-o" aria-hidden="true"></i> Input Harian Harga Komoditi</a></li>
                                <!--? endif; ?-->
                            </ul>
                        </li>
                    </ul>





                </section>
                <!-- /.sidebar -->
            </aside>
            <?php
        }
        ?>





      <!-- =============================================== -->
      <!-- Ganti Password -->
      <div class="modal modal-info fade" id="xchange-password" >
        <div class="modal-dialog modal-sm">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h4 class="modal-title"><i class="fa fa-key" aria-hidden="true"></i> Ganti Password</h4>
            </div>
            <form id="xchange-password-form">
              <div class="modal-body">
                <div class="form-group">
                  <input type="password" class="form-control" id="inputPasswordLama" name="inputPasswordLama" placeholder="Password Lama" maxlength="16">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" id="inputPasswordBaru" name="inputPasswordBaru" placeholder="Password Baru" maxlength="16">
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" id="inputKonfirmasi" name="inputKonfirmasi" placeholder="Konfirmasi" maxlength="16">
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Simpan</button>
                <button type="button" class="btn btn-sm btn-flat btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Batal</button>
              </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- =============================================== -->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
