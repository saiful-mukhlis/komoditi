<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Content Wrapper. Contains page content -->
  <section class="content-header">
    <h1>
      <i class="fa fa-times-circle text-red"></i> Error Hak Akses
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li class="active"><i class="fa fa-times-circle text-red"></i> Error Hak Akses</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="error-page">
      <div class="row">
        <div class="col-md-4 text-center">
          <p class="">
            <img src="images/logo.png?_=<?=time()?>" style="width: 152px;"><br/>
            <small>Pemerintah Kabupaten Sidoarjo</small>
          </p>
        </div>
        <div class="col-md-8 text-center">
          <div class="error-content">
            <h3><i class="fa fa-times-circle text-red"></i> Maaf, anda tidak memiliki hak akses</h3>
            <p><?=$description?></p>
          </div>
        </div>
      </div>
      <!-- /.error-content -->
    </div>
    <!-- /.error-page -->
  </section>
