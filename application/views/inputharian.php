<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Pasar</h1>
  <ol class="breadcrumb">
    <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Transaksi Pasar</a></li>
    <li class="active">Input Harga Harian Komoditi</li>
  </ol>
</section>

<!-- Main content -->
<div class="container" id="container">
      <!-- Main component for a primary marketing message or call to action -->
          


<script type="text/javascript">
// $( document ).ready(function() {
//   // autocomplete
//   $('#datepicker').datetimepicker({
//     format: 'yyyy-mm-dd',
//     autoclose: true,
//     minView: 2,
//     startView: 2,
//     pickTime: false
//   });
//   pilihPasar();
// });
//
//
// function pilihPasar(){
//
//     var val = $('#kabkota').val();
//     var val = 'malangkota'
//
//
//     $('select[name=psr_id]').empty();
//     $.getJSON('http://siskaperbapo.com/harga/pasar.json/'+val,function(data){
//           html = '<option value="">- PILIH PASAR -</option>';
//           $('select[name=psr_id]').append(html);
//         $.each(data,function(i,psr){
//            html = '<option value="' + psr.psr_id + '">' + psr.psr_nama + '</option>';
//            $('select[name=psr_id]').append(html);
//         });
//         $('#pasar option[value=""]').attr("selected",true);
//     });
//
//   }
//
// function getLastData(){
//     var pasar = $('#pasar').val();
//     if(pasar==''){
//         alert('Pasar belum dipilih');
//     }else{
//
//       $('input[id^="frm-"]').val('');
//
//       $.getJSON('admin.php?action=konsumen&do=lastdata_json&pasar='+pasar,function(data){
//           $.each(data,function(i,itm){
//              $('#frm-'+itm.hg_bahanpokok).val(itm.hg_harga);
//           });
//       });
//     }
// }
//
// function updatededata(){
//    val = $('#pasar').val();
//
//    if(val==''){
//         alert('pasar belum dipilih');
//         return false;
//    }else{
//       if(confirm('Data sudah benar?')){
//           return true;
//       }else{
//         return false;
//       }
//    }
// }


</script>
<h1 id="overview-doctype" class="page-header">Tambah Harga Konsumen</h1>

<form id="form1" action="" method="post">

<div class="row">
<div class="col-md-8">


  <div class="well" style="padding:10px;">
 
                  <div class="form-group">
                    <label for="exampleInputEmail2">Tanggal : </label>
                      <div class="input-group date" id="datepicker" data-provide="datepicker">
                        <input type="text" class="form-control" id="tanggal" name="tanggal" value="<?php echo date('Y-m-d') ?>"  readonly="">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        
                      </div>
                  </div>
                                               <div class="form-group">
                    <label for="exampleInputEmail3">&nbsp; Pasar : </label>
                                                   <select class="form-control" name="psr_id" id="pasar">
                                                       <?php
                                                       foreach ($model['pasar'] as $item) {
                                                           ?>
                                                           <option value="<?php echo $item->kode ?>"> <?php echo $item->nama ?> </option>
                                                           <?php
                                                       }
                                                       ?>
                                                   </select>
                    
                  </div>                                   

  </div>


  <div>*) Mohon membersihkan tanda titik(.) atau koma(,) yang biasa digunakan sebagai pembatas ribuan sebelum memasukkan harga. Masukkan hanya angka saja tanpa atribut apapun.</div>
  <table class="table table-bordered table-hover">
   <tbody>
   <tr>
    <th style="width:10px">No</th>
    <th style="width:10px">Kode</th>
    <th>Nama</th>         
    <th style="width:100px">Satuan</th>         
    <th style="width:100px">Stok</th>
    <th style="width:100px">Harga</th>
   </tr>

   <?php
   foreach ($model['komoditi'] as $no => $item) {
       ?>
       <tr>
           <td><?php echo ($no+1) ?></td>
           <td><?php echo $item->kodekelompok . str_replace('BRG0', '', $item->kode) ?></td>
           <td><?php echo $item->nama ?></td>
           <td><?php echo $item->kodesatuan_text ?></td>
           <td><input type="text" name="stok[<?php echo $item->kode ?>]" value="" style="width:75px;"></td>
           <td><input type="text" name="harga[<?php echo $item->kode ?>]" value="" style="width:75px;"></td>
       </tr>
       <?php
   }
   ?>

              
           
  <tr>
    <td></td>
    <td></td>
    <td><input type="submit" name="submit" class="btn btn-primary" onclick="return confirm('Data Sudah benar?')" value="Update Data" style="float:left"></td>   
    <td></td>
    <td></td>
  </tr>  
  </tbody></table>
</div>
</div>
</form>    </div>
<!-- /.content -->
