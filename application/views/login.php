<?php
/***********************************************
 ***   NEW RECONSTRUCTION DONE - 24/10/2017  ***
 ***********************************************/
defined("BASEPATH") OR exit("No direct script access allowed");

?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Informasi Komoditi Bahan Pokok</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- BEGIN CSS -->
    <?=CSSList()?>
    <!-- END CSS -->
    <!-- FAVICON -->
    <link rel="shortcut icon" href="<?= base_url("favicon.ico") ?>">
    <style type="text/css">
      .backgroundbody {
        background-image: url("<?=base_url("images/prototype4.jpg")?>");
        background-repeat: no-repeat;
        background-position: center;
      }
      .splashTitle {
        color: #EEEEEE;
        text-shadow: -2px 0 black, 0 2px black, 2px 0 black, 0 -2px black; /* horizontal-offset vertical-offset 'blur' colour */
        -moz-text-shadow: -2px 0 black, 0 2px black, 2px 0 black, 0 -2px black;
        -webkit-text-shadow: -2px 0 black, 0 2px black, 2px 0 black, 0 -2px black;
      }
      /*button i.fa { padding-right: 4px; }*/
    </style>
  </head>
  <body class="hold-transition login-page backgroundbody">
    <div class="login-box">
      <div class="login-logo" style="margin-bottom: 10px; color: #000;">
        <img src="<?=base_url("images/logo.png")?>?_=<?=time()?>" width="200"><br/>
        <b class="splashTitle">SIKBP </b>
      </div>
      <!-- /.login-logo -->
      <div class="login-box-body">
        <form id="formLogin" autocomplete="off">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="UID" id="inputUID" name="inputUID" autocomplete="off">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" id="inputPassword" name="inputPassword" autocomplete="off">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in" aria-hidden="true"></i> Sign In</button>
            </div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <!-- BEGIN JAVASCRIPT -->
    <script type="text/javascript">var xpathctrl="<?=$this->pathctrl?>/";</script>
    <?=JavascriptList()?>
    <!-- END JAVASCRIPT -->
  </body>
</html>
