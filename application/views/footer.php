<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> <?=CI_VERSION?>
        </div>
        <strong>Copyright &copy; <a href="http://www.mitrasolusiprima.co.id/" target="_blank">Mitra Solusi Prima</a>, <?= date("Y") ?>.</strong> All rights reserved.
      </footer>
    </div>
    <!-- ./wrapper -->
    <!-- BEGIN JAVASCRIPT -->
    <script type="text/javascript">var xpathctrl="<?=$this->pathctrl?>/", xpath="<?=$this->path?>";</script>
    <?=JavascriptList()?>
    <!-- END JAVASCRIPT -->
  </body>
</html>
