<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>Satuan</h1>
  <ol class="breadcrumb">
    <li><a href="./"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="#">Data Master</a></li>
    <li class="active">Satuan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

  <!-- List View box -->
  <div class="box xlist-view">
    <div class="box-body no-padding table-responsive">
      <table class="table table-hover">
        <thead>
          <tr>
            <th>Satuan</th>
            <th class="text-right">Kontrol</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
      <button id="tombolTambah" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah</button>
    </div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
  <!-- Form Tambah Edit box -->
  <div class="box xform-tambah-edit hidden">
    <div class="box-header with-border">
      <h3 class="box-title"></h3>
    </div>
    <form class="form-horizontal">
      <input type="hidden" id="inputOldSatuan" name="inputOldSatuan">
      <div class="box-body">
        <div class="col-sm-6">
          <div class="form-group">
            <label for="inputSatuan" class="col-sm-4 control-label">Satuan</label>
            <div class="col-sm-8">
              <input type="text" class="form-control input-sm" id="inputSatuan" name="inputSatuan">
            </div>
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Simpan</button>
        &nbsp;&nbsp;
        <button type="button" class="btn btn-default btn-sm xbatal"><i class="fa fa-times"></i>&nbsp;&nbsp;Batal</button>
      </div>
      <!-- /.box-footer-->
    </form>
  </div>
  <!-- /.box -->
</section>
<!-- /.content -->
