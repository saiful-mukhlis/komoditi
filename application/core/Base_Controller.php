<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_Controller extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    if ($this->IsLogin) {
      /*
      $this->EditAllow = TRUE;
      $this->data['cssextra'][] = 'assets/adminlte/css/skins/_all-skins.min.css';
      $this->data['cssextra'][] = 'css/global.css?_='.time();
      $this->data['jsextra'][]= 'assets/slimscroll/jquery.slimscroll.min.js';
      $this->data['jsextra'][]= 'assets/adminlte/js/app.min.js';
      $this->data['jsextra'][]= 'js/dashboard.js?_='.time();
      $this->PageAllowed = $this->am->CheckPageAllowed();
      $this->PageTimelineAllowed = $this->am->CheckPageTimelineAllowed();
      */
    } else redirect(base_url());
  }
}
//
class Ajaxbase_Controller extends MY_Controller
{
  public function __construct()
  {
    parent::__construct();
    //header('Content-type: application/json; charset=utf-8');
    //header("Access-Control-Allow-Origin: *");
    if (!$this->IsAjax) exit('No direct script access allowed');
  }
}
//
class Ajaxmember_Controller extends Ajaxbase_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->IsLogin) exit('No login data access allowed');
   // if (!$this->am->CheckChildAllowed()) exit('No access allowed');
   // $this->PageTimelineAllowed = $this->am->CheckChildTimelineAllowed();
  }
}
