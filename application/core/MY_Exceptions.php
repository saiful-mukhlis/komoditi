<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class dealing with errors as exceptions
 */
class CIException extends Exception {
    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }
}

class MY_Exceptions extends CI_Exceptions {
  /**
   * Force exception throwing on erros
   */
  public function show_error($heading, $message, $template = 'error_general', $status_code = 200)
  {
    set_status_header($status_code);
    $message = implode(" / ", (!is_array($message)) ? array($message) : $message);
    throw new CIException($message);
  }
}

/**
 * Captured error from Code Igniter
 */

/**
 * My Custom Error Exception
 */
