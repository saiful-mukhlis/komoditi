<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//require_once 'SSRSReport.php';

class MY_Controller extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    /********************************************************/
    $this->load->library("session");
    $this->load->library("pagememory",NULL,"pm");
    $this->load->helper(array("url", "myhtml"));
    /********************************************************/
    $this->IsAjax = $this->input->is_ajax_request();
    $this->IsIndex = TRUE;
    $this->IsLogin = FALSE;
    $this->ctrl = $this->router->fetch_class();
    $this->path = "";
    $tpart = explode("/", uri_string());
    $tisctrl = FALSE;
    foreach($tpart AS $item) {
      if ($item !== "childs") {
        if ($item !== $this->ctrl) {
          if ($tisctrl === FALSE) {
            if (!empty($this->path)) $this->path .= "/";
            $this->path .= $item;
          } else $this->IsIndex = FALSE;
        } else $tisctrl = TRUE;
      }
    }
    if (!empty($this->path) && substr($this->path,-1) != "/") $this->path .= "/";
    $this->pathctrl = $this->path.$this->ctrl;
    log_message("INFO", "Path : {$this->pathctrl}, URI : ".uri_string().", Is Index : {$this->IsIndex} ");
    /********************************************************/
    if (!$this->IsAjax) {
      $this->data["cssextra"] = array(
        "assets/bootstrap/css/bootstrap.min.css",
        "assets/font-awesome/css/font-awesome.min.css",
        "assets/adminlte/css/adminlte.min.css",
        "css/{$this->path}global.css?_=".time());
      $this->data["jsextra"] = array(
        "assets/jquery/jquery-2.2.3.min.js",
        "assets/bootstrap/js/bootstrap.min.js",
        "js/{$this->path}global.js?_=".time());
    }
    /**/
    //$this->XPERIODE = $this->pm->Periode();
    /**/
    /*if (!empty($this->XPERIODE)) {
      $this->load->config("siksda{$this->XPERIODE}");
      $dbconfig = array(
        "dsn" => "",
        "hostname" => $this->config->item('db_hostname'),
        "username" => $this->config->item('db_username'),
        "password" => $this->config->item('db_password'),
        "database" => $this->config->item('db_database'),
        "dbdriver" => "sqlsrv",
        "dbprefix" => "",
        "pconnect" => FALSE,
        "db_debug" => (ENVIRONMENT !== "production"),
        "cache_on" => FALSE,
        "cachedir" => "",
        "char_set" => "utf8",
        "dbcollat" => "utf8_general_ci",
        "swap_pre" => "",
        "encrypt" => FALSE,
        "compress" => FALSE,
        "stricton" => FALSE,
        "failover" => array(),
        "save_queries" => TRUE
      );

      $this->load->database($dbconfig);
      $this->load->library("am");
    */
      $this->load->model("{$this->path}global_model", "xmdlg");

      //$this->data["apps"] = $this->xmdlg->app_setting();
      $this->data["user"] = $this->xmdlg->user_setting();
      //$this->appconf = $this->xmdlg->app_config();
      /*
      $this->data["periodetitle"] = "";
      switch($this->data["apps"]->timeline) {
        case 1: $this->data["periodetitle"] = "Pagu Indikatif"; break;
        case 2: $this->data["periodetitle"] = "Penyusunan Anggaran (RKA)"; break;
        case 3: $this->data["periodetitle"] = "Penetapan Anggaran (DPA)"; break;
        case 4: $this->data["periodetitle"] = "Perubahan Perbup"; break;
        case 5: $this->data["periodetitle"] = "Pagu Indikatif Perubahan"; break;
        case 6: $this->data["periodetitle"] = "Perubahan Anggaran (PAK)"; break;
        case 7: $this->data["periodetitle"] = "Penetapan Perubahan Anggaran (DPPA)"; break;
        case 8: $this->data["periodetitle"] = "Perubahan Perbup PAK"; break;
      }*/
      $this->IsLogin = !empty($this->data["user"]);
      //$this->IsPAK = ($this->data["apps"]->timeline >= $this->appconf->timeline_pak) || ($this->data["apps"]->isperubahan == 1) ;
      //
      if ($this->IsLogin && !$this->IsAjax) {
        $this->EditAllow = TRUE;
        $this->data["cssextra"][] = "assets/adminlte/css/skins/_all-skins.min.css";
        $this->data["jsextra"][]= "assets/slimscroll/jquery.slimscroll.min.js";
        $this->data["jsextra"][]= "assets/adminlte/js/app.min.js";
        $this->data["jsextra"][]= "js/{$this->path}dashboard.js?_=".time();
        //$this->PageAllowed = $this->am->CheckPageAllowed();
        //$this->PageTimelineAllowed = $this->am->CheckPageTimelineAllowed();
      }
    //}
    if ($this->IsAjax) {
      header("Content-type: application/json; charset=utf-8");
      header("Access-Control-Allow-Origin: *");
      if ($this->IsLogin) {
        /*if ($this->am->CheckChildAllowed()) {
          $this->PageTimelineAllowed = $this->am->CheckChildTimelineAllowed();
        }*/
      }
    }
  }
}
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
class MY_Wsserver extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    //
    define("WSUSERNAME", "BankJatim");
    define("WSPASSWORD", "12SdA+JatiM90");
    /**/
    $this->load->library("session");
    $this->load->library("pagememory",NULL,"pm");
    $this->load->library("decrypt");
    $this->load->helper(array("url"));
    /**/
    $this->ctrl = $this->router->fetch_class();
    $tpart = explode("/", uri_string());
    $this->path = $tpart[0];
    if ($this->path == "childs") $this->path = "";
    if ($this->path == $this->ctrl) $this->path = "";
    if (!empty($this->path) && strpos($this->path, "/") === FALSE) $this->path .= "/";
    $this->pathctrl = $this->path.$this->ctrl;
    $this->appjson = (isset($_SERVER['CONTENT_TYPE']) && $_SERVER['CONTENT_TYPE'] == "application/json");
    /**/
    header('Content-type: application/json; charset=utf-8');
    header("Access-Control-Allow-Origin: *");
    if (!$this->appjson) {
      $tJSON["ResponseCode"] = "03";
      $tJSON["isError"] = TRUE;
      $tJSON["ErrorDesc"] = "Bad Request";
      $tJSON["StatusTransaksi"] = 0;
      log_message("ERROR", "[~~~:: RequireContent-type: application/json ::~~~]");
      $tXJSON["Response"] = $this->decrypt->Encryption(json_encode($tJSON));
      exit(json_encode($tXJSON));
    }
    if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']) && 
        $_SERVER['PHP_AUTH_USER'] === WSUSERNAME && 
        $_SERVER['PHP_AUTH_PW'] === WSPASSWORD) {
      $this->XPERIODE = date("Y");
      /**/
      $this->load->config('siksda'.$this->XPERIODE);
      $dbconfig = array(
        'dsn' => '',
        'hostname' => $this->config->item('db_hostname'),
        'username' => $this->config->item('db_username'),
        'password' => $this->config->item('db_password'),
        'database' => $this->config->item('db_database'),
        'dbdriver' => 'sqlsrv',
        'dbprefix' => '',
        'pconnect' => FALSE,
        'db_debug' => (ENVIRONMENT !== 'production'),
        'cache_on' => FALSE,
        'cachedir' => '',
        'char_set' => 'utf8',
        'dbcollat' => 'utf8_general_ci',
        'swap_pre' => '',
        'encrypt' => FALSE,
        'compress' => FALSE,
        'stricton' => FALSE,
        'failover' => array(),
        'save_queries' => TRUE
      );
      $this->load->database($dbconfig);
    } else {
      $tJSON["ResponseCode"] = "01";
      $tJSON["isError"] = TRUE;
      $tJSON["ErrorDesc"] = "Access Denied";
      $tJSON["StatusTransaksi"] = 0;
      log_message("ERROR", "[~~~:: Username or Password Authorization Error ::~~~]");
      $tXJSON["Response"] = $this->decrypt->Encryption(json_encode($tJSON));
      exit(json_encode($tXJSON));
    }
    
  }
}
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
class MY_Wsclient extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    //
    $this->load->library("session");
    $this->load->library("pagememory",NULL,"pm");
    $this->load->library("restclient");
    $this->load->library("decrypt");
    $this->load->helper(array("url"));
    /**/
    $this->ctrl = $this->router->fetch_class();
    $tpart = explode("/", uri_string());
    $this->path = $tpart[0];
    if ($this->path == "childs") $this->path = "";
    if ($this->path == $this->ctrl) $this->path = "";
    if (!empty($this->path) && strpos($this->path, "/") === FALSE) $this->path .= "/";
    $this->pathctrl = $this->path.$this->ctrl;
    /**/
    $WS = $this->pm->WebService();
    /**/
    header('Content-type: application/json;');
    header("Access-Control-Allow-Origin: *");
    /**/
    $this->XPERIODE = date("Y");
    /**/
    $this->load->config('siksda'.$this->XPERIODE);
    $dbconfig = array(
      'dsn' => '',
      'hostname' => $this->config->item('db_hostname'),
      'username' => $this->config->item('db_username'),
      'password' => $this->config->item('db_password'),
      'database' => $this->config->item('db_database'),
      'dbdriver' => 'sqlsrv',
      'dbprefix' => '',
      'pconnect' => FALSE,
      'db_debug' => (ENVIRONMENT !== 'production'),
      'cache_on' => FALSE,
      'cachedir' => '',
      'char_set' => 'utf8',
      'dbcollat' => 'utf8_general_ci',
      'swap_pre' => '',
      'encrypt' => FALSE,
      'compress' => FALSE,
      'stricton' => FALSE,
      'failover' => array(),
      'save_queries' => TRUE
    );
    $this->load->database($dbconfig);
  }
}
//
require APPPATH."core/Base_Controller.php";
/*require APPPATH."core/Rekening_Controller.php";
require APPPATH."core/Program_Controller.php";
require APPPATH."core/Kegiatan_Controller.php";
require APPPATH."core/Pagu_Controller.php";
require APPPATH."core/Rob_Controller.php";
require APPPATH."core/Rrob_Controller.php";
require APPPATH."core/Dpa_Controller.php";
require APPPATH."core/Rkbmd_Controller.php";
require APPPATH."core/Rkbmdverify_Controller.php";
require APPPATH."core/Rkbmdverpus_Controller.php";

require APPPATH."core/Jurnal_Controller.php";
*/









/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
///   NEW STRUCTURE CONTROLLER   ////////////////////////
/////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
class MY_Xcontroller extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    /********************************************************/
    $this->load->library("session");
    $this->load->library("pagememory",NULL,"pm");
    $this->load->helper(array("url", "myhtml"));
    /********************************************************/
      //log_message("INFO",  print_r($_SESSION,true));
    $this->IsAjax = $this->input->is_ajax_request();
    $this->IsIndex = TRUE;
    $this->IsLogin = FALSE;
    $this->ctrl = $this->router->fetch_class();
    $this->path = "";
    $tpart = explode("/", uri_string());
    $tisctrl = FALSE;
    foreach($tpart AS $item) {
      if ($item !== $this->ctrl) {
        if ($tisctrl === FALSE) {
          if (!empty($this->path)) $this->path .= "/";
          $this->path .= $item;
        } else $this->IsIndex = FALSE;
      } else $tisctrl = TRUE;
    }
    if (!empty($this->path) && substr($this->path,-1) != "/") $this->path .= "/";
    $this->pathctrl = $this->path.$this->ctrl;
    //log_message("INFO", "Path : {$this->pathctrl}, URI : ".uri_string().", Is Index : {$this->IsIndex} ");
    /********************************************************/
    //$this->XPERIODE = $this->pm->Periode();
    //if (!empty($this->XPERIODE)) {
      /*$this->load->config("siksda{$this->XPERIODE}");
      $dbconfig = array(
        "dsn" => "",
        "hostname" => $this->config->item('db_hostname'),
        "username" => $this->config->item('db_username'),
        "password" => $this->config->item('db_password'),
        "database" => $this->config->item('db_database'),
        "dbdriver" => "sqlsrv",
        "dbprefix" => "",
        "pconnect" => FALSE,
        "db_debug" => TRUE,
        "cache_on" => FALSE,
        "cachedir" => "",
        "char_set" => "utf8",
        "dbcollat" => "utf8_general_ci",
        "swap_pre" => "",
        "encrypt" => FALSE,
        "compress" => FALSE,
        "stricton" => FALSE,
        "failover" => array(),
        "save_queries" => TRUE
      );*/
      /**/
      //$this->load->database($dbconfig);
      $this->load->model("{$this->path}global_model", "xmdlg");
      //$this->data["apps"] = $this->xmdlg->app_setting();
      $this->data["user"] = $this->xmdlg->user_setting();
      //$this->appconf = $this->xmdlg->app_config();
      $this->IsLogin = !empty($this->data["user"]);
      //$this->IsPAK = ($this->data["apps"]->timeline >= $this->appconf->timeline_pak);
      //$this->IsPerubahan = ($this->data["apps"]->isperubahan == 1);
        log_message('info',print_r($this->data["user"],true));
    //}
    if ($this->IsAjax) {
      header("Content-type: application/json; charset=utf-8");
      header("Access-Control-Allow-Origin: *");
    } else {
      $this->data["cssextra"] = array(
        "assets/bootstrap/css/bootstrap.min.css",
        "assets/font-awesome/css/font-awesome.min.css",
        "assets/adminlte/css/adminlte.min.css",
        "css/{$this->path}global.css?_=".time());
      $this->data["jsextra"] = array(
        "assets/jquery/jquery-2.2.3.min.js",
        "assets/bootstrap/js/bootstrap.min.js",
        "js/{$this->path}global.js?_=".time());
    }
  }
}
////
class MY_Xnonmember extends MY_Xcontroller
{
  public function __construct()
  {
    parent::__construct();
    if ($this->IsIndex) {
      (!$this->IsLogin) OR redirect(base_url("{$this->path}dashboard"));
    } else {
      (!$this->IsLogin && $this->IsAjax) OR exit("No direct script access allowed...");
    }
  }
}
////
class MY_Xmember extends MY_Xcontroller
{
    
    private function hak()
    {
        if (!empty($_SESSION['xuserlogin'])) {
            $data = $this->db->get_where('tmpetugas', array('uid' => $_SESSION['xuserlogin']), 1, 0)->result();
            if(empty($data[0])){
                return false;
            }else{
                if(empty($data[0]->hak)){
                    return 'admin';
                }else{
                    return $data[0]->hak;
                }
            }
        }
        return false;
    }
    
  public function __construct()
  {
    parent::__construct();

    $GLOBALS['u_hak'] = $this->data['u_hak'] = $this->hak();

    //$this->load->library("am");
    $this->load->model("{$this->path}dashboard_model", "xmdldsbrd");
    ($this->ctrl == "dashboard") OR $this->load->helper("datatable");
    if ($this->IsIndex) {
      ($this->IsLogin) OR redirect(base_url("{$this->path}"));
      //$this->data["periodetitle"] = "";
      $this->EditAllow = TRUE;
      $this->data["cssextra"][] = "assets/adminlte/css/skins/_all-skins.min.css";
      $this->data["jsextra"][]= "assets/slimscroll/jquery.slimscroll.min.js";
      $this->data["jsextra"][]= "assets/adminlte/js/app.min.js";
      if ($this->ctrl !== "dashboard") {
        $this->data["cssextra"][] = "assets/jqueryui/themes/smoothness/jquery-ui.min.css";
        $this->data["cssextra"][] = "assets/datatablesc/datatables.min.css";
        $this->data["cssextra"][] = "assets/datatablesc/datatables/css/dataTables.jqueryui.min.css";
        $this->data["cssextra"][] = "assets/datatablesc/fixedheader/css/fixedHeader.jqueryui.min.css";
        $this->data["cssextra"][] = "assets/datatablesc/responsive/css/responsive.jqueryui.min.css";
        $this->data["cssextra"][] = "assets/datatablesc/rowgroup/css/rowGroup.jqueryui.min.css";
        $this->data["cssextra"][] = "assets/datatablesc/scroller/css/scroller.jqueryui.min.css";
        $this->data["cssextra"][] = "assets/datatablesc/select/css/select.jqueryui.min.css";
        $this->data["jsextra"][] = "assets/datatablesc/datatables.min.js";
      }
      $this->data["jsextra"][]= "js/{$this->path}dashboard.js?_=".time();
    } else {
      ($this->IsLogin && $this->IsAjax) OR exit("No direct script access allowed...");
    }
  }
}

//require APPPATH."core/Rekeningx_Controller.php";
class MyException extends Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }
}
