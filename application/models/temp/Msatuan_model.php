<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class Msatuan_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  //
  public function load_list_satuan()
  {
    $tSQL = "SELECT * FROM tmsatuan";
    return $this->db->query($tSQL)->result();
  }
  //
  public function tambah_edit_satuan($pzData)
  {
    $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
    if ($pzData['inputOldSatuan'] == '') {
      $tSQL = "INSERT INTO tmsatuan (satuan) VALUES (?)";
      $tParam = array($pzData['inputSatuan']);
    } else {
      $tSQL = "UPDATE tmsatuan SET satuan = ? WHERE (satuan = ?)";
      $tParam = array($pzData['inputSatuan'], $pzData['inputOldSatuan']);
    }
    $this->db->query($tSQL, $tParam);
    $tMsg = $this->db->error();
    $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
    if ($tMsg['code'] == '00000') return true;
    return false;
  }
  //
  public function hapus_satuan($pzData)
  {
    $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
    $tSQL = "DELETE FROM tmsatuan WHERE (satuan = ?)";
    $tParam = array($pzData['satuan']);
    $this->db->query($tSQL, $tParam);
    $tMsg = $this->db->error();
    $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
    if ($tMsg['code'] == '00000') return true;
    return false;
  }
}
