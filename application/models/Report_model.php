<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class Report_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  //

  public function list_data()
  {
    $PM = $this->pm->Memory();
    $kodePasar = $GLOBALS['u_hak'];
    $tSQL = "SELECT * FROM ttkomoditi where kodepasar='{$kodePasar}'";
    $tTemp = datatablepaging($tSQL);
    return $tTemp;
  }

    public function list_data_view($date)
    {
        if (empty($date)) {
            $date = $this->date();
        }
        $this->db->select('c.*');
        $this->db->select('tmlogkomoditi.*, a.nama as kodesatuan_text, b.nama as kodekelompok_text, c.*');
        $this->db->from('tmlogkomoditi');
//        $this->db->from('ttkomiditidetail as c');
        $this->db->join('ttkomiditidetail as c', 'tmlogkomoditi.kode = c.kodekomoditi');
        $this->db->join('ttkomoditi as d', 'd.kode = c.kodetrans');
        $this->db->join('tmlogreference as a', 'a.kode = tmlogkomoditi.kodesatuan');
        $this->db->join('tmlogreference as b', 'b.kode = tmlogkomoditi.kodekelompok');


        $this->db->where('d.tanggal', $date);
        return $this->db->get()->result();
    }

    public function buildTitle()
    {
        $kelompok = $this->kelompok();

        foreach ($kelompok as &$item) {
            $item->child = $this->kelompokDetail($item->kode);
            $item->satuan_text = $this->kelompokDetail($item->kode);
        }
        


        return $kelompok;
    }

    public function kelompok()
    {
        return $this->db->like('kode', 'K', 'after')->get('tmlogreference')->result();
    }

    public function satuan()
    {
        $data = $this->db->like('kode', 'S', 'after')->get('tmlogreference')->result();

        $tmp = [];
        foreach ($data as $k=>$item) {
            $tmp[$item->kode] = $item;
        }


        return $tmp;
    }

    public function kelompokDetail($kode)
    {
        return $this->db->get_where('tmlogkomoditi', array('kodekelompok' => $kode))->result();
    }

    public function date()
    {
        if(!empty($_GET['kode'])){
            $tmp = $this->db->get_where('ttkomoditi', array('kode' => $_GET['kode']))->result();
            if (!empty($tmp[0])) {
                return $tmp[0]->tanggal;
            }
        }else{
            return date('Y-m-d') . ' 00:00:00';
        }
    }
}
