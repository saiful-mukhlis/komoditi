<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class MBank_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  //
  public function list_data()
  {
    $PM = $this->pm->Memory();
    $tSQL = "SELECT kode, nama FROM TMBank WHERE (1 = 1)";
    $WhereMap = array("kode" => "kode", "nama" => "nama");
    $tTemp = datatablepaging($this->input->post(), $tSQL, $WhereMap, "nama");
    return $tTemp;
  }
  //
  public function update_saldo_awal($pzData)
  {
    $thasil = 1000;
    $PM = $this->pm->Memory();
    $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
    $tSQL = "SELECT b.dk FROM TTJurnal a INNER JOIN TTJurnalD b ON (a.IdJurnal = b.IdJurnal) AND (b.IdDJurnal = ?) WHERE (a.Aktif = 0)";
    $tParam = array($pzData['idd']);
    $tcheck = $this->db->query($tSQL, $tParam)->row();
    if (isset($tcheck)) {
      $tDK = $tcheck->dk == 'D' ? "Debet" : "Kredit";
      $tSQL = "UPDATE TTJurnalD SET {$tDK} = ?, Saldo = ? WHERE (IdDJurnal = ?)";
      $tParam = array($pzData['inpsa'], $pzData['inpsa'], $pzData['idd']);
      $this->db->query($tSQL, $tParam);
      $thasil = 0;
    }
    $tMsg = $this->db->error();
    $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
    if ($thasil !== 0) return thasil;
    if ($tMsg['code'] == '00000') return TRUE;
    return FALSE;
  }
  
  //
  public function proc_verify()
  {
    $thasil = 0;
    $PM = $this->pm->Memory();
    $tIDAnggaran = $this->data['apps']->periode.$PM['skpd'];
    $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
    // Check Balance
    $tSQL = "SELECT ISNULL(SUM(Debet), 0) AS totdebet, ISNULL(SUM(Kredit), 0) AS totkredit FROM TTJurnalD a INNER JOIN TTJurnal b ON (a.IdJurnal = b.IdJurnal) WHERE (b.IdJurnal = 'GN{$tIDAnggaran}')";
    $tCheck = $this->db->query($tSQL)->row();
    if ($tCheck->totdebet == 0 && $tCheck->totkredit == 0) {
      $thasil = 1000;
    } elseif ($tCheck->totdebet == $tCheck->totkredit) {
      $tSQL = "UPDATE TTJurnal SET Aktif = 1 WHERE (Aktif = 0) AND (IdJurnal = 'GN{$tIDAnggaran}')";
      $this->db->query($tSQL);
      if ($this->db->affected_rows() == 0) {
        $thasil = 1200;
      } else {
        $tSQL = "EXEC [dbo].[UpSaldo] 1, 'GN{$tIDAnggaran}'";
        $this->db->query($tSQL);
      }
      $tMsg = $this->db->error();
    } else {
      $thasil = 1100;
    }
    $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
    if ($thasil !== 0) return $thasil;
    if ($tMsg['code'] == '00000') return TRUE;
    return FALSE;
  }
  //
  public function proc_unverify()
  {
    $thasil = 0;
    $PM = $this->pm->Memory();
    $tIDAnggaran = $this->data['apps']->periode.$PM['skpd'];
    $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
    $tSQL = "UPDATE TTJurnal SET Aktif = 0 WHERE (Aktif = 1) AND (IdJurnal = 'GN{$tIDAnggaran}')";
    $this->db->query($tSQL);
    if ($this->db->affected_rows() == 0) {
      $thasil = 1000;
    } else {
      $tSQL = "EXEC [dbo].[UpSaldo] 0, 'GN{$tIDAnggaran}'";
      $this->db->query($tSQL);
    }
    $tMsg = $this->db->error();
    $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
    if ($thasil !== 0) return $thasil;
    if ($tMsg['code'] == '00000') return TRUE;
    return FALSE;
  }
  
  
  
  
  
  
  //
  public function list_permen64()
  {
    $PM = $this->pm->Memory();
    $tSQL = "SELECT tahunkode AS id, TahunKode+' - '+Nama AS permen64 FROM TMRekening64 WHERE (Tingkat = 5) AND (TahunKode LIKE '113%')";
    $WhereMap = array("permen64" => "TahunKode+' - '+Nama");
    $tTemp = datatablepaging($this->input->post(), $tSQL, $WhereMap, "TahunKode");
    return $tTemp;
  }
}
