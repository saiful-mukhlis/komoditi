<?php
/***********************************************
 ***   NEW RECONSTRUCTION DONE - 24/10/2017  ***
 ***********************************************/
defined("BASEPATH") OR exit("No direct script access allowed");

//-- CHECKED OK --//
class Dashboard_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  //
  public function update_password($pzData)
  {
    try {
      $UID = $this->session->userdata("xuserlogin");
      $tSQL = "SELECT * FROM tmpetugas WHERE (uid = ?)";
      $x = $this->db->query($tSQL, array($UID));
      $tRow = $x->row();
      if ($tRow->pwd != md5($pzData["inputPasswordLama"])){
        throw new MyException("Password lama salah...", 1);
      } else {
        $tSQL = "UPDATE tmpetugas SET pwd = ? WHERE (uid = ?)";
        $tParam = array(md5($pzData["inputPasswordBaru"]), $UID);
        $this->db->query($tSQL, $tParam);
      }
    } catch (Exception $ex) {
      throw $ex;
    }
  }
}
