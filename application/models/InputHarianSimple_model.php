<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class InputHarianSimple_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list_pasar()
    {
        // doc ada di sini -> https://www.codeigniter.com/userguide3/database/query_builder.html
//        return $this->db->get('tmorgpasar')->result();
        if($GLOBALS['u_hak'] == 'admin'){
            return $this->db->get('tmorgpasar')->result();
        }else{
            return $this->db->get_where('tmorgpasar', array('kode' => $GLOBALS['u_hak']), 1, 0)->result();
        }

    }

    public function list_komoditi()
    {
        return $this->db->get('tmlogkomoditi')->result();
    }

    public function list_komoditi_relation()
    {
        // https://www.codeigniter.com/userguide3/database/query_builder.html
        $this->db->select('tmlogkomoditi.*, a.nama as kodesatuan_text, b.nama as kodekelompok_text');
        $this->db->from('tmlogkomoditi');
        $this->db->join('tmlogreference as a', 'a.kode = tmlogkomoditi.kodesatuan');
        $this->db->join('tmlogreference as b', 'b.kode = tmlogkomoditi.kodekelompok');
        return $this->db->get()->result();
    }

    public function data()
    {
        return [
            'pasar' => $this->list_pasar(),
            'komoditi' => $this->list_komoditi_relation(),
        ];
    }

    public function save($param)
    {
        if (
        !empty($param['tanggal'])
        ) {

            $id = $this->insert_ttkomoditi($param);




            foreach ($param['stok'] as $key => $item) {
                if (!empty($param['harga'][$key]) && !empty($item)) {
                    $this->insert_detail($id, $key, $item, $param['harga'][$key]);
                }
            }
        }

        header("Location: listdata?id=" . $id);
        die();

    }

    private function pasar_kode()
    {
        if($GLOBALS['u_hak'] == 'admin'){
            return $_POST['kode'];
        }else{
            return $GLOBALS['u_hak'];
        }
    }

    private function insert_ttkomoditi($param)
    {
        $data = array(
            'tanggal' => $param['tanggal'],
            'kodepasar' => $this->pasar_kode(),
        );

        $this->db->insert('ttkomoditi', $data);

        return $this->db->insert_id();
    }

    private function insert_detail($id, $kodekomoditi, $stok, $harga)
    {
        $data = array(
            'kodetrans' => $id,
            'kodekomoditi' => $kodekomoditi,
            'stok' => $stok,
            'harga' => $harga,
        );

        $this->db->insert('ttkomiditidetail', $data);


    }

}
