<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class Global_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  //
  public function app_setting()
  {
    $tSQL = "SELECT Periode, TL, Perubahan, IsPerubahan, ".
            "CONVERT(VARCHAR(20), BatasTanggal, 111) AS BatasTanggal ".
            "FROM TSPAnggaran WHERE (Tipe = 1)";
    $query = $this->db->query($tSQL);
           ;//$this->db->get_where('TSPAnggaran', array('Tipe' => 1));
    $value = $query->row();
    if (!empty($value)) {
      $data['periode'] = $value->Periode;
      $data['timeline'] = $value->TL;
      $data['perubahan'] = $value->Perubahan;
      $data['isperubahan'] = $value->IsPerubahan;
      $data['batastanggal'] = empty($value->BatasTanggal) ? $value->Periode."/01/01 00:00:00" : $value->BatasTanggal." 00:00:00";
      $query = $this->db->get('TMPemda');
      $value = $query->row();
      $data['bupati'] = !empty($value->Bupati) ? $value->Bupati : '';
      $data['wilayah'] = "Pemerintah " . $value->NamaPemda;
    } else {
      $data['periode'] = '';
      $data['timeline'] = 0;
      $data['perubahan'] = 0;
      $data['isperubahan'] = 0;
      $data['batastanggal'] = '2017/01/01';
      $data['bupati'] = '';
      $data['wilayah'] = '';
    }
    return (object)$data;
  }
  //
  public function user_setting()
  {
    $sess = $this->session->userdata('xuserlogin');
      //log_message('info',$sess);
    if (!empty($sess)) {
      $query = $this->db->get_where('tmpetugas', array('uid' => $sess));
      $value = $query->row();
      if (!empty($value))
      {
        /*$skpd = null;
        if (!empty($value->KodeSatker) && $value->KodeSatker != 'all') {
          $query = $this->db->get_where('TMSatker', array('KodeSatker' => $value->KodeSatker));
          $skpd = $query->row();
          $data['namaskpd'] = $skpd->Nama;
          $data['stptu'] = empty($skpd->StPtu) ? '0' : '1';
          $data['stsah'] = empty($skpd->StPengesahan) ? '0' : '1';
        } else {
          $data['namaskpd'] = '';
          $data['stptu'] = $value->KodeSatker == 'all' ? '1' : '0';
          $data['stsah'] = $value->KodeSatker == 'all' ? '1' : '0';
        }*/
        $data['kode'] = $value->kode;
        $data['nama'] = $value->nama;
        $data['hak'] = $value->hak;
        //$data['skpd'] = $value->KodeSatker;
          //log_message("info",print_r($data,true));
          return (object)$data;
      }
    }
    return null;
  }
  //
  public function app_config()
  {
    $tResult = array();
    $query = $this->db->get('TSKonfig');
    foreach ($query->result() as $row)
    {
      if (strpos($row->konfig, '_timeline') !== false) {
        $tResult[$row->konfig] = explode(",", $row->nilai);
      } elseif (strpos($row->konfig, 'bud_') !== false) {
        $tResult[$row->konfig] = $row->nilai;
      } else {
        $tResult[$row->konfig] = (int)$row->nilai;
      }
    }
    return (object)$tResult;
  }
}
