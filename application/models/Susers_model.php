<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class Susers_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  //
  private function kode_generator()
  {
    $tMasking = "PTG" . date("ym") . "____";
    $tCount = 1;
    $tSQL = "SELECT * FROM tmpetugas WHERE (kode LIKE ?) ORDER BY kode DESC";
    $x = $this->db->query($tSQL, array($tMasking));
    if ($x->num_rows() > 0) {
      $tRow = $x->row();
      $tCount = (int)substr($tRow->Kode, 7, 4);
      $tCount += 1;
    }
    return  "PTG" . date("ym") . str_pad($tCount, 4, "0", STR_PAD_LEFT);
  }
  //
  public function list_option_skpd()
  {
    $tSQL = "SELECT * FROM TMSatker WHERE (Detil = 1 OR StPTU = 1) AND (KodeSatker LIKE '_______%')";
    return $this->db->query($tSQL)->result();
  }
  //
  public function load_list_users()
  {
    $tSQL = "SELECT a.kode, a.nama, a.`uid`, a.hak, a.kodesatker, a.kodesatker + ' ' + ISNULL(b.Nama, '') AS satker FROM tmpetugas a LEFT OUTER JOIN TMSatker b ON (a.KodeSatker = b.KodeSatker)";
    return $this->db->query($tSQL)->result();
  }
  //
//  public function tambah_edit_users($pzData)
//  {
//    $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
//    if ($pzData['inputKode'] == '') {
//      $tKode = $this->kode_generator();
//      $this->load->helper('string');
//      $tPassword = random_string('alnum', 16);
//      $tSQL = "INSERT INTO TMPetugas (Kode, UID, Nama, PWD, Hak, KodeSatker) VALUES (?, ?, ?, ?, ?, ?)";
//      $tParam = array($tKode, $pzData['inputUID'], $pzData['inputNama'], md5($tPassword), $pzData['inputHakAkses'], $pzData['inputSKPD']);
//    } else {
//      $tPassword = "";
//      $tSQL = "UPDATE TMPetugas SET UID = ?, Nama = ?, Hak = ?, KodeSatker = ? WHERE (Kode = ?)";
//      $tParam = array($pzData['inputUID'], $pzData['inputNama'], $pzData['inputHakAkses'], $pzData['inputSKPD'], $pzData['inputKode']);
//    }
//    $this->db->query($tSQL, $tParam);
//    $tMsg = $this->db->error();
//    $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
//    if ($tMsg['code'] == '00000') return array("password" => $tPassword);
//    return false;
//  }
  //

    public function hapus($param)
    {
        if (!empty($param['kode'])) {
            $this->db->delete('tmpetugas', array('uid' => $param['kode']));
            return true;
        }
        return false;

    }

  public function hapus_users($pzKode)
  {
    $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
    $tSQL = "DELETE FROM tmpetugas WHERE (kode = ?)";
    $tParam = array($pzKode);
    $this->db->query($tSQL, $tParam);
    $tMsg = $this->db->error();
    $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
    if ($tMsg['code'] == '00000') return true;
    return false;
  }
  //
  public function reset_password_users($pzKode)
  {
    $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
    $this->load->helper('string');
    $tPassword = random_string('alnum', 16);
    $tSQL = "UPDATE tmpetugas SET pwd = ? WHERE (kode = ?)";
    $tParam = array(md5($tPassword), $pzKode);
    $this->db->query($tSQL, $tParam);
    $tMsg = $this->db->error();
    $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
    if ($tMsg['code'] == '00000') return array("password" => $tPassword);
    return false;
  }
  //
  public function update_password($pzData)
  {
    $UID = $this->session->userdata('xuserlogin');
    $tSQL = "SELECT * FROM tmpetugas WHERE (uid = ?)";
    $x = $this->db->query($tSQL, array($UID));
    $tRow = $x->row();
    if ($tRow->pwd != md5($pzData['inputPasswordLama'])) return false;
    else {
      $tSQL = "UPDATE tmpetugas SET pwd = ? WHERE (uid = ?)";
      $tParam = array(md5($pzData['inputPasswordBaru']), $UID);
      $this->db->query($tSQL, $tParam);
    }
    return true;
  }

  private function pasar($kode)
  {
      return $this->db->get_where('tmorgpasar', array('kode' => $kode), 1, 0)->result();
  }

  public function list_data()
  {
      $PM = $this->pm->Memory();
      $tSQL = "SELECT * FROM tmpetugas where 1=1";
      $tTemp = datatablepaging($tSQL);
      
//      echo '<pre>';
//      print_r($tTemp['data']);
//      echo '</pre>';
//      exit;

      foreach ($tTemp['data'] as $k => $item) {
          if (empty($item->hak)) {
              $tTemp['data'][$k]->hak = 'Semua pasar';
              $tTemp['data'][$k]->hak_id = 0;
          }else{
              $pasar = $this->pasar($item->hak);
              if(!empty($pasar[0])){
                  $tTemp['data'][$k]->hak_id = $tTemp['data'][$k]->hak;
                  $tTemp['data'][$k]->hak = $pasar[0]->nama;
              }

          }


          if($item->aktif){
              $tTemp['data'][$k]->aktif = 'Aktif';
          }else{
              $tTemp['data'][$k]->aktif = 'Tidak Aktif';
          }

      }
      

      
      return $tTemp;
  }

  private function check($uid)
  {
      return $this->db->get_where('tmpetugas', array('uid' => $uid), 1, 0)->result();
  }



  public function add($param)
  {

      if(empty($param['idedit'])){
          if(empty($this->check($param['inputUid']))){
              $data = array(
                  'nama' => $param['inputName'],
                  'kode' => $param['inputUid'],
                  'uid' => $param['inputUid'],
                  'pwd' => md5($param['inputPassword']),
                  'hak' => $param['inputHak'],
                  'aktif' => 1,
                  'stbatal' => 1,
              );

              $this->db->insert('tmpetugas', $data);

              return $param['inputUid'];
          }
      }else{
          // get
          $result = $this->check($param['idedit']);
          if($result[0]->uid == $param['inputUid']){
              //update
              $data = array(
                  'nama' => $param['inputName'],
                  'kode' => $param['inputUid'],
                  'uid' => $param['inputUid'],
                  'pwd' => md5($param['inputPassword']),
                  'hak' => $param['inputHak'],
                  'aktif' => 1,
                  'stbatal' => 1,
              );
              $this->db->where('uid', $param['idedit']);
              $this->db->update('tmpetugas', $data);

              return true;
          }else{
              if(empty($this->check($param['inputUid']))){
                  $data = array(
                      'nama' => $param['inputName'],
                      'kode' => $param['inputUid'],
                      'uid' => $param['inputUid'],
                      'pwd' => md5($param['inputPassword']),
                      'hak' => $param['inputHak'],
                      'aktif' => 1,
                      'stbatal' => 1,
                  );

                  $this->db->where('uid', $param['idedit']);
                  $this->db->update('tmpetugas', $data);

                  return true;
              }
          }

      }



      return false;


  }
}
