<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class Pasar_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  //
  public function list_data_all()
  {
    $PM = $this->pm->Memory();
    $tSQL = "SELECT kode, nama, alamat FROM tmorgpasar where 1=1";
    $tTemp = datatablepaging(null);
    return $tTemp;
  }
  public function list_data()
  {
    $PM = $this->pm->Memory();
    $tSQL = "SELECT kode, nama, alamat FROM tmorgpasar where 1=1";
    $tTemp = datatablepaging($tSQL);
    return $tTemp;
  }
  //
    public function kode_generator()
    {
        $depan = "P";
        $tCount = 1;
        $tSQL = "SELECT kode FROM tmorgpasar WHERE 1=1 ORDER BY kode DESC";
        $x = $this->db->query($tSQL);
        if ($x->num_rows() > 0) {
            $tRow = $x->row();
            $tCount = (int)substr($tRow->kode, -2);
            $tCount += 1;
        }
        return $depan.str_pad($tCount, 2, "0", STR_PAD_LEFT);
    }
    //
    public function tambah_edit_pasar($pzData)
    {
        $tKode = "";
        $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
        if ($pzData['idedit'] == '') {
            $tKode = $this->kode_generator();
            $tSQL = "INSERT INTO tmorgpasar (kode, nama, alamat) VALUES (?, ?, ?)";
            $tParam = array($tKode, $pzData['inputNama'], $pzData['inputAlamat']);
            $this->db->query($tSQL, $tParam);
            ////////
            $tMemPage = $this->pm->Memory();
            $tMemPage['id'] = $tKode;
            $this->pm->Memory($tMemPage);
        } else {
            $tKode = $pzData['idedit'];
            $tSQL = "UPDATE tmorgpasar SET nama = ?, alamat = ? WHERE (kode = ?)";
            $tParam = array($pzData['inputNama'], $pzData['inputAlamat'], $tKode);
            $this->db->query($tSQL, $tParam);
        }
        $tMsg = $this->db->error();
        $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
        if ($tMsg['code'] == '00') return $tKode;
        return FALSE;
    }
    //
    public function hapus_pasar($pzKode)
    {
        $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
        $tSQL = "DELETE FROM tmorgpasar WHERE (kode = ?)";
        $tParam = array($pzKode);
        $this->db->query($tSQL, $tParam);
        $tMsg = $this->db->error();
        $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
        if ($tMsg['code'] == '00') return TRUE;
        return FALSE;
    }
}
