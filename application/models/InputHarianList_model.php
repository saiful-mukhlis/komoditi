<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class InputHarianList_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function list_result()
    {
        // doc ada di sini -> https://www.codeigniter.com/userguide3/database/query_builder.html
        $this->db->select('ttkomiditidetail.*, b.nama as pasar_nama');
        $this->db->from('ttkomiditidetail');
        $this->db->join('ttkomoditi as a', 'a.kode = ttkomiditidetail.kodetrans');
        $this->db->join('tmlogkomoditi as e', 'e.kode = ttkomiditidetail.kodekomoditi');

        $this->db->join('tmorgpasar as b', 'b.kode = a.kodepasar');
        $this->db->join('tmlogreference as c', 'c.kode = tmlogkomoditi.kodesatuan');
        $this->db->join('tmlogreference as d', 'd.kode = tmlogkomoditi.kodekelompok');
        return $this->db->get()->result();
    }



}
