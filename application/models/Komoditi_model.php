<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-- CHECKED OK --//
class Komoditi_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }
  //
  public function list_data()
  {
    $PM = $this->pm->Memory();
    $tSQL = "SELECT a.kode, a.nama, b.nama as kelompok, c.nama as satuan FROM tmlogkomoditi a inner join tmlogreference b on a.kodekelompok = b.kode inner join tmlogreference c on a.kodesatuan = c.kode where 1=1";
    $tTemp = datatablepaging($tSQL);
    return $tTemp;
  }
  //
    public function kode_generator()
    {
        $depan = "BRG";
        $tCount = 1;
        $tSQL = "SELECT kode FROM tmlogkomoditi WHERE 1=1 ORDER BY kode DESC";
        $x = $this->db->query($tSQL);
        if ($x->num_rows() > 0) {
            $tRow = $x->row();
            $tCount = (int)substr($tRow->kode, -3);
            $tCount += 1;
        }
        return $depan.str_pad($tCount, 3, "00", STR_PAD_LEFT);
    }
    //
    public function load_option_kelompok()
    {
        $tSQL = "SELECT kode, nama FROM tmlogreference where tipe = 0 ORDER BY nama";
        return $this->db->query($tSQL)->result();
    }

    public function load_option_satuan()
    {
        $tSQL = "SELECT kode, nama FROM tmlogreference where tipe = 1 ORDER BY nama";
        return $this->db->query($tSQL)->result();
    }

    public function tambah_edit_komoditi($pzData)
    {
        $tKode = "";
        $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
        if ($pzData['idedit'] == '') {
            $tKode = $this->kode_generator();
            $tSQL = "INSERT INTO tmlogkomoditi (kode, nama, kodekelompok, kodesatuan) VALUES (?, ?, ?, ?)";
            $tParam = array($tKode, $pzData['inputNama'], $pzData['inputKelompok'], $pzData['inputSatuan']);
            $this->db->query($tSQL, $tParam);
            ////////
            $tMemPage = $this->pm->Memory();
            $tMemPage['id'] = $tKode;
            $this->pm->Memory($tMemPage);
        } else {
            $tKode = $pzData['idedit'];
            $tSQL = "UPDATE tmlogkomoditi SET nama = ?, kodekelompok = ?, kodesatuan = ? WHERE (kode = ?)";
            $tParam = array($pzData['inputNama'], $pzData['inputKelompok'], $pzData['inputSatuan'], $tKode);
            $this->db->query($tSQL, $tParam);
        }
        $tMsg = $this->db->error();
        $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
        if ($tMsg['code'] == '00') return $tKode;
        return FALSE;
    }
    //
    public function hapus_komoditi($pzKode)
    {
        $this->db->db_debug = FALSE; // DISABLE DB ERROR REPORT
        $tSQL = "DELETE FROM tmlogkomoditi WHERE (kode = ?)";
        $tParam = array($pzKode);
        $this->db->query($tSQL, $tParam);
        $tMsg = $this->db->error();
        $this->db->db_debug = TRUE; // DISABLE DB ERROR REPORT
        if ($tMsg['code'] == '00') return TRUE;
        return FALSE;
    }
}
