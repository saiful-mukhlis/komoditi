<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Globalengine
{

  protected $CI;

  public function __construct()
  {
    $this->CI =& get_instance();
  }
  //
  private function SetSimulasi()
  {
    $this->CI->session->unset_userdata('xuserlogin');
  }
  // OK
  public function CountField($pzTable, $pzCriteria = '')
  {
    $tSQL = "SELECT COUNT(*) AS hslCount FROM $pzTable";
    if ($pzCriteria != '') $tSQL .= " WHERE $pzCriteria";
    $x = $this->CI->db->query($tSQL);
    return $x->row()->hslCount;
  }
  // OK
  public function SumField($pzTable, $pzField, $pzCriteria = '')
  {
    $tSQL = "SELECT ISNULL(SUM($pzField), 0) AS hslSum FROM $pzTable";
    if ($pzCriteria != '') $tSQL .= " WHERE $pzCriteria";
    $x = $this->CI->db->query($tSQL);
    return $x->row()->hslSum;
  }
  // OK
  public function GetFieldValue($pzTable, $pzField, $pzCriteria)
  {
    $tSQL = "SELECT $pzField AS xField FROM $pzTable WHERE $pzCriteria";
    $x = $this->CI->db->query($tSQL);
    if ($x->num_rows() == 0) return false;
    return $x->row()->xField;
  }
  // OK
  public function GetNextIndex($pzTable, $pzField, $pzCriteria = '')
  {
    $tSQL = "SELECT ISNULL(MAX($pzField), 0) + 1 AS nextID FROM $pzTable";
    if ($pzCriteria != '') $tSQL .= " WHERE $pzCriteria";
    $x = $this->CI->db->query($tSQL);
    return $x->row()->nextID;
  }
  //
  public function LoadSetting()
  {
    $tHasil = null;
    $tSSUL = $this->CI->session->userdata('xuserlogin');
    //log_message('info', $tSSUL);
    if (is_null($tSSUL) == false) {
      $tSQL = "SELECT * FROM TMPetugas WHERE (UID = ?)";
      $x = $this->CI->db->query($tSQL, array($tSSUL));
      $tRow = $x->row();
      $tHasil = array("kode" => $tRow->Kode, "nama" => $tRow->Nama, "hak" => $tRow->Hak);
    }
    return $tHasil;
  }

}
