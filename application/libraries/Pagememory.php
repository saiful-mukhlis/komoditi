<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagememory
{
  public function __get($var)
  {
    return get_instance()->$var;
  }
  //
  public function Memory($pzObject = NULL)
  {
    $ssname = str_replace("/", "", "xpm".$this->path);
    $PM = $this->session->userdata($ssname);
    if (gettype($pzObject) == "array" && empty($pzObject)) $pzObject = NULL;
    if (!isset($PM)) $PM = array();
    if (!isset($PM[$this->ctrl])) $PM[$this->ctrl] = NULL;
    if (isset($pzObject) && $PM[$this->ctrl] != $pzObject) $PM[$this->ctrl] = $pzObject;
    $this->session->set_userdata($ssname, $PM);
    return $PM[$this->ctrl];
  }
  //
  public function Clear()
  {
    $ssname = str_replace("/", "", "xpm".$this->path);
    $PM = $this->session->userdata($ssname);
    if (isset($PM[$this->ctrl])) $PM[$this->ctrl] = NULL;
    $this->session->set_userdata($ssname, $PM);
  }
  //
  public function Close()
  {
    $ssname = str_replace("/", "", "xpm".$this->path);
    $this->session->unset_userdata($ssname);
  }
  //
  public function Push($pzObject)
  {
    $ssname = str_replace("/", "", "xps".$this->path);
    $tStack = $this->session->userdata($ssname);
    if (is_null($tStack)) $tStack = array();
    array_push($tStack, $pzObject);
    $this->session->set_userdata($ssname, $tStack);
  }
  //
  public function Pop()
  {
    $tHasil = null;
    $ssname = str_replace("/", "", "xps".$this->path);
    $tStack = $this->session->userdata($ssname);
    if (!is_null($tStack)) $tHasil = array_pop($tStack);
    $this->session->set_userdata($ssname, $tStack);
    return $tHasil;
  }
  //
  public function Info()
  {
    $tHasil = null;
    $ssname = str_replace("/", "", "xps".$this->path);
    $tStack = $this->session->userdata($ssname);
    if (!is_null($tStack)) $tHasil = array_pop($tStack);
    return $tHasil;
  }
  //
  public function ClearStack()
  {
    $ssname = str_replace("/", "", "xps".$this->path);
    $this->session->unset_userdata($ssname);
  }
  //
  public function Periode($pzPeriode = NULL)
  {
    $ssname = str_replace("/", "", "xprd".$this->path);
    $PM = $this->session->userdata($ssname);
    if (isset($pzPeriode) && $PM != $pzPeriode) $PM = $pzPeriode;
    $this->session->set_userdata($ssname, $PM);
    return $PM;
  }
  //
  public function ClearPeriode()
  {
    $ssname = str_replace("/", "", "xprd".$this->path);
    $this->session->unset_userdata($ssname);
  }
  //
  public function WebService($pzWebService = NULL)
  {
    $ssname = str_replace("/", "", "xws".$this->path);
    $PM = $this->session->userdata($ssname);
    if (isset($pzWebService) && $PM != $pzWebService) $PM = $pzWebService;
    $this->session->set_userdata($ssname, $PM);
    return $PM;
  }
  //
  public function ClearWebService()
  {
    $ssname = str_replace("/", "", "xws".$this->path);
    $this->session->unset_userdata($ssname);
  }
}
